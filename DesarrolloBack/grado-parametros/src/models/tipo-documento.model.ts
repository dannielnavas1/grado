import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class TipoDocumento extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idTipoDocumento?: number;

  @property({
    type: 'string',
    required: true,
  })
  TipoDocumento: string;


  constructor(data?: Partial<TipoDocumento>) {
    super(data);
  }
}

export interface TipoDocumentoRelations {
  // describe navigational properties here
}

export type TipoDocumentoWithRelations = TipoDocumento & TipoDocumentoRelations;
