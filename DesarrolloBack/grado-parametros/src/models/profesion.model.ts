import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Profesion extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  idProfesion: number;

  @property({
    type: 'string',
    required: true,
  })
  Profesion: string;


  constructor(data?: Partial<Profesion>) {
    super(data);
  }
}

export interface ProfesionRelations {
  // describe navigational properties here
}

export type ProfesionWithRelations = Profesion & ProfesionRelations;
