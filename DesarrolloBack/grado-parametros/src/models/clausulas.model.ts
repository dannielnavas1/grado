import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Clausulas extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idClausulas?: number;

  @property({
    type: 'string',
    required: true,
  })
  TipoClausula: string;

  @property({
    type: 'string',
    required: true,
  })
  Clausulas: string;


  constructor(data?: Partial<Clausulas>) {
    super(data);
  }
}

export interface ClausulasRelations {
  // describe navigational properties here
}

export type ClausulasWithRelations = Clausulas & ClausulasRelations;
