import {Entity, model, property} from '@loopback/repository';

@model()
export class Ciudad extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
    generated: false,
  })
  idCiudad: number;

  @property({
    type: 'string',
    required: true,
  })
  Ciudad: string;

  @property({
    type: 'number',
    required: true,
  })
  Departamento_idDepartamento: number;


  constructor(data?: Partial<Ciudad>) {
    super(data);
  }
}

export interface CiudadRelations {
  // describe navigational properties here
}

export type CiudadWithRelations = Ciudad & CiudadRelations;
