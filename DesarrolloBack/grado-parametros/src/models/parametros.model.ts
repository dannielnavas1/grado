import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Parametros extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idParametros?: number;

  @property({
    type: 'string',
    required: true,
  })
  Parametros: string;


  constructor(data?: Partial<Parametros>) {
    super(data);
  }
}

export interface ParametrosRelations {
  // describe navigational properties here
}

export type ParametrosWithRelations = Parametros & ParametrosRelations;
