import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { AlmacenContratos } from '../models';
import { AlmacenContratosRepository } from '../repositories';

export class AlmacenContratosController {
  constructor(
    @repository(AlmacenContratosRepository)
    public almacenContratosRepository: AlmacenContratosRepository,
  ) { }

  @post('/api/almacencontratos', {
    responses: {
      '200': {
        description: 'AlmacenContratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(AlmacenContratos) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AlmacenContratos, { exclude: ['idAlmacenContratos'] }),
        },
      },
    })
    almacenContratos: Omit<AlmacenContratos, 'idAlmacenContratos'>,
  ): Promise<AlmacenContratos> {
    return this.almacenContratosRepository.create(almacenContratos);
  }

  @get('/api/almacencontratos/count', {
    responses: {
      '200': {
        description: 'AlmacenContratos model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(AlmacenContratos)) where?: Where<AlmacenContratos>,
  ): Promise<Count> {
    return this.almacenContratosRepository.count(where);
  }

  @get('/api/almacencontratos', {
    responses: {
      '200': {
        description: 'Array of AlmacenContratos model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(AlmacenContratos) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(AlmacenContratos)) filter?: Filter<AlmacenContratos>,
  ): Promise<AlmacenContratos[]> {
    return this.almacenContratosRepository.find(filter);
  }

  @patch('/api/almacencontratos', {
    responses: {
      '200': {
        description: 'AlmacenContratos PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AlmacenContratos, { partial: true }),
        },
      },
    })
    almacenContratos: AlmacenContratos,
    @param.query.object('where', getWhereSchemaFor(AlmacenContratos)) where?: Where<AlmacenContratos>,
  ): Promise<Count> {
    return this.almacenContratosRepository.updateAll(almacenContratos, where);
  }

  @get('/api/almacencontratos/{id}', {
    responses: {
      '200': {
        description: 'AlmacenContratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(AlmacenContratos) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<AlmacenContratos> {
    return this.almacenContratosRepository.findById(id);
  }


  @get('/api/almacencontratosid/{id}', {
    responses: {
      '200': {
        description: 'AlmacenContratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(AlmacenContratos) } },
      },
    },
  })
  async findByIdCon(@param.path.number('id') id: number): Promise<object> {
    const miContrato = await this.almacenContratosRepository.find({ 'where': { 'TipoContrato_idTipoContrato': id } });
    console.info(miContrato);
    if (!miContrato) {
      console.warn(miContrato);
      console.error('No se encontraron registros asociados');
      throw new HttpErrors.NotFound('No se encuentra registrados')
    }
    console.info('La consulta trae datos');
    return miContrato;
  }

  @patch('/api/almacencontratos/{id}', {
    responses: {
      '204': {
        description: 'AlmacenContratos PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AlmacenContratos, { partial: true }),
        },
      },
    })
    almacenContratos: AlmacenContratos,
  ): Promise<void> {
    await this.almacenContratosRepository.updateById(id, almacenContratos);
  }

  @put('/api/almacencontratos/{id}', {
    responses: {
      '204': {
        description: 'AlmacenContratos PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() almacenContratos: AlmacenContratos,
  ): Promise<void> {
    await this.almacenContratosRepository.replaceById(id, almacenContratos);
  }

  @del('/api/almacencontratos/{id}', {
    responses: {
      '204': {
        description: 'AlmacenContratos DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.almacenContratosRepository.deleteById(id);
  }
}
