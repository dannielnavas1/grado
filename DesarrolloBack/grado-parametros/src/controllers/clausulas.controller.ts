import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Clausulas } from '../models';
import { ClausulasRepository } from '../repositories';

export class ClausulasController {
  constructor(
    @repository(ClausulasRepository)
    public clausulasRepository: ClausulasRepository,
  ) { }

  @post('/api/clausulas', {
    responses: {
      '200': {
        description: 'Clausulas model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Clausulas) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Clausulas, { exclude: ['idClausulas'] }),
        },
      },
    })
    clausulas: Omit<Clausulas, 'idClausulas'>,
  ): Promise<Clausulas> {
    return this.clausulasRepository.create(clausulas);
  }

  @get('/api/clausulas/count', {
    responses: {
      '200': {
        description: 'Clausulas model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Clausulas)) where?: Where<Clausulas>,
  ): Promise<Count> {
    return this.clausulasRepository.count(where);
  }

  @get('/api/clausulas', {
    responses: {
      '200': {
        description: 'Array of Clausulas model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Clausulas) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Clausulas)) filter?: Filter<Clausulas>,
  ): Promise<Clausulas[]> {
    return this.clausulasRepository.find(filter);
  }

  @patch('/api/clausulas', {
    responses: {
      '200': {
        description: 'Clausulas PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Clausulas, { partial: true }),
        },
      },
    })
    clausulas: Clausulas,
    @param.query.object('where', getWhereSchemaFor(Clausulas)) where?: Where<Clausulas>,
  ): Promise<Count> {
    return this.clausulasRepository.updateAll(clausulas, where);
  }

  @get('/api/clausulas/{id}', {
    responses: {
      '200': {
        description: 'Clausulas model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Clausulas) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Clausulas> {
    return this.clausulasRepository.findById(id);
  }

  @patch('/api/clausulas/{id}', {
    responses: {
      '204': {
        description: 'Clausulas PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Clausulas, { partial: true }),
        },
      },
    })
    clausulas: Clausulas,
  ): Promise<void> {
    await this.clausulasRepository.updateById(id, clausulas);
  }

  @put('/api/clausulas/{id}', {
    responses: {
      '204': {
        description: 'Clausulas PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() clausulas: Clausulas,
  ): Promise<void> {
    await this.clausulasRepository.replaceById(id, clausulas);
  }

  @del('/api/clausulas/{id}', {
    responses: {
      '204': {
        description: 'Clausulas DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.clausulasRepository.deleteById(id);
  }
}
