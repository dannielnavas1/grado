import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Parametros } from '../models';
import { ParametrosRepository } from '../repositories';

export class ParametrosController {
  constructor(
    @repository(ParametrosRepository)
    public parametrosRepository: ParametrosRepository,
  ) { }

  @post('/api/parametros', {
    responses: {
      '200': {
        description: 'Parametros model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Parametros) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Parametros, { exclude: ['idParametros'] }),
        },
      },
    })
    parametros: Omit<Parametros, 'idParametros'>,
  ): Promise<Parametros> {
    return this.parametrosRepository.create(parametros);
  }

  @get('/api/parametros/count', {
    responses: {
      '200': {
        description: 'Parametros model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Parametros)) where?: Where<Parametros>,
  ): Promise<Count> {
    return this.parametrosRepository.count(where);
  }

  @get('/api/parametros', {
    responses: {
      '200': {
        description: 'Array of Parametros model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Parametros) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Parametros)) filter?: Filter<Parametros>,
  ): Promise<Parametros[]> {
    return this.parametrosRepository.find(filter);
  }

  @patch('/api/parametros', {
    responses: {
      '200': {
        description: 'Parametros PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Parametros, { partial: true }),
        },
      },
    })
    parametros: Parametros,
    @param.query.object('where', getWhereSchemaFor(Parametros)) where?: Where<Parametros>,
  ): Promise<Count> {
    return this.parametrosRepository.updateAll(parametros, where);
  }

  @get('/api/parametros/{id}', {
    responses: {
      '200': {
        description: 'Parametros model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Parametros) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Parametros> {
    return this.parametrosRepository.findById(id);
  }

  @patch('/api/parametros/{id}', {
    responses: {
      '204': {
        description: 'Parametros PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Parametros, { partial: true }),
        },
      },
    })
    parametros: Parametros,
  ): Promise<void> {
    await this.parametrosRepository.updateById(id, parametros);
  }

  @put('/api/parametros/{id}', {
    responses: {
      '204': {
        description: 'Parametros PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() parametros: Parametros,
  ): Promise<void> {
    await this.parametrosRepository.replaceById(id, parametros);
  }

  @del('/api/parametros/{id}', {
    responses: {
      '204': {
        description: 'Parametros DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.parametrosRepository.deleteById(id);
  }
}
