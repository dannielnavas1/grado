import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { TipoContrato } from '../models';
import { TipoContratoRepository } from '../repositories';

export class TipoContratoController {
  constructor(
    @repository(TipoContratoRepository)
    public tipoContratoRepository: TipoContratoRepository,
  ) { }

  @post('/api/tipocontratos', {
    responses: {
      '200': {
        description: 'TipoContrato model instance',
        content: { 'application/json': { schema: getModelSchemaRef(TipoContrato) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoContrato, { exclude: ['idTipoContrato'] }),
        },
      },
    })
    tipoContrato: Omit<TipoContrato, 'idTipoContrato'>,
  ): Promise<TipoContrato> {
    return this.tipoContratoRepository.create(tipoContrato);
  }

  @get('/api/tipocontratos/count', {
    responses: {
      '200': {
        description: 'TipoContrato model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(TipoContrato)) where?: Where<TipoContrato>,
  ): Promise<Count> {
    return this.tipoContratoRepository.count(where);
  }

  @get('/api/tipocontratos', {
    responses: {
      '200': {
        description: 'Array of TipoContrato model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(TipoContrato) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(TipoContrato)) filter?: Filter<TipoContrato>,
  ): Promise<TipoContrato[]> {
    return this.tipoContratoRepository.find(filter);
  }

  @patch('/api/tipocontratos', {
    responses: {
      '200': {
        description: 'TipoContrato PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoContrato, { partial: true }),
        },
      },
    })
    tipoContrato: TipoContrato,
    @param.query.object('where', getWhereSchemaFor(TipoContrato)) where?: Where<TipoContrato>,
  ): Promise<Count> {
    return this.tipoContratoRepository.updateAll(tipoContrato, where);
  }

  @get('/api/tipocontratos/{id}', {
    responses: {
      '200': {
        description: 'TipoContrato model instance',
        content: { 'application/json': { schema: getModelSchemaRef(TipoContrato) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<TipoContrato> {
    return this.tipoContratoRepository.findById(id);
  }

  @patch('/api/tipocontratos/{id}', {
    responses: {
      '204': {
        description: 'TipoContrato PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoContrato, { partial: true }),
        },
      },
    })
    tipoContrato: TipoContrato,
  ): Promise<void> {
    await this.tipoContratoRepository.updateById(id, tipoContrato);
  }

  @put('/api/tipocontratos/{id}', {
    responses: {
      '204': {
        description: 'TipoContrato PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() tipoContrato: TipoContrato,
  ): Promise<void> {
    await this.tipoContratoRepository.replaceById(id, tipoContrato);
  }

  @del('/api/tipocontratos/{id}', {
    responses: {
      '204': {
        description: 'TipoContrato DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.tipoContratoRepository.deleteById(id);
  }
}
