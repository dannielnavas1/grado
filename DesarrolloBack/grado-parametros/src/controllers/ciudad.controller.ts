import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Ciudad } from '../models';
import { CiudadRepository } from '../repositories';

export class CiudadController {
  constructor(
    @repository(CiudadRepository)
    public ciudadRepository: CiudadRepository,
  ) { }

  @post('/ciudades', {
    responses: {
      '200': {
        description: 'Ciudad model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Ciudad) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ciudad),
        },
      },
    })
    ciudad: Omit<Ciudad, 'idCiudad'>,
  ): Promise<Ciudad> {
    return this.ciudadRepository.create(ciudad);
  }

  @get('/ciudades/count', {
    responses: {
      '200': {
        description: 'Ciudad model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Ciudad)) where?: Where<Ciudad>,
  ): Promise<Count> {
    return this.ciudadRepository.count(where);
  }

  @get('/ciudades', {
    responses: {
      '200': {
        description: 'Array of Ciudad model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Ciudad) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Ciudad)) filter?: Filter<Ciudad>,
  ): Promise<Ciudad[]> {
    return this.ciudadRepository.find(filter);
  }

  @patch('/ciudades', {
    responses: {
      '200': {
        description: 'Ciudad PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ciudad, { partial: true }),
        },
      },
    })
    ciudad: Ciudad,
    @param.query.object('where', getWhereSchemaFor(Ciudad)) where?: Where<Ciudad>,
  ): Promise<Count> {
    return this.ciudadRepository.updateAll(ciudad, where);
  }

  @get('/ciudades/{id}', {
    responses: {
      '200': {
        description: 'Ciudad model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Ciudad) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Ciudad> {
    return this.ciudadRepository.findById(id);
  }

  @patch('/ciudades/{id}', {
    responses: {
      '204': {
        description: 'Ciudad PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ciudad, { partial: true }),
        },
      },
    })
    ciudad: Ciudad,
  ): Promise<void> {
    await this.ciudadRepository.updateById(id, ciudad);
  }

  @put('/ciudades/{id}', {
    responses: {
      '204': {
        description: 'Ciudad PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() ciudad: Ciudad,
  ): Promise<void> {
    await this.ciudadRepository.replaceById(id, ciudad);
  }

  @del('/ciudades/{id}', {
    responses: {
      '204': {
        description: 'Ciudad DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.ciudadRepository.deleteById(id);
  }
}
