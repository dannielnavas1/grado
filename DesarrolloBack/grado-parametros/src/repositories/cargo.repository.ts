import {DefaultCrudRepository} from '@loopback/repository';
import {Cargo, CargoRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CargoRepository extends DefaultCrudRepository<
  Cargo,
  typeof Cargo.prototype.idCargo,
  CargoRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Cargo, dataSource);
  }
}
