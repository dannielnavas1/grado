import {DefaultCrudRepository} from '@loopback/repository';
import {Parametros, ParametrosRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ParametrosRepository extends DefaultCrudRepository<
  Parametros,
  typeof Parametros.prototype.idParametros,
  ParametrosRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Parametros, dataSource);
  }
}
