import {DefaultCrudRepository} from '@loopback/repository';
import {Sector, SectorRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SectorRepository extends DefaultCrudRepository<
  Sector,
  typeof Sector.prototype.idSector,
  SectorRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Sector, dataSource);
  }
}
