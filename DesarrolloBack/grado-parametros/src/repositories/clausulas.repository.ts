import {DefaultCrudRepository} from '@loopback/repository';
import {Clausulas, ClausulasRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ClausulasRepository extends DefaultCrudRepository<
  Clausulas,
  typeof Clausulas.prototype.idClausulas,
  ClausulasRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Clausulas, dataSource);
  }
}
