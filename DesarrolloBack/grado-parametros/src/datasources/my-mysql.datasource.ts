import { inject } from '@loopback/core';
import { juggler } from '@loopback/repository';
import * as config from './my-mysql.datasource.json';

export class MyMysqlDataSource extends juggler.DataSource {
  static dataSourceName = 'MyMysql';

  constructor(
    @inject('datasources.config.MyMysql', { optional: true })
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
