import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Persona extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  idPersona: number;

  @property({
    type: 'string',
    required: true,
  })
  Nombre: string;

  @property({
    type: 'string',
    required: true,
  })
  Apellido: string;

  @property({
    type: 'string',
    required: true,
  })
  Direccion: string;

  @property({
    type: 'string',
    required: true,
  })
  FirmaContratoPersona: string;

  @property({
    type: 'number',
    required: true,
  })
  Profesion_idProfesion: number;

  @property({
    type: 'number',
    required: true,
  })
  Pais_idPais: number;

  @property({
    type: 'number',
    required: true,
  })
  Departamento_idDepartamento: number;

  @property({
    type: 'number',
    required: true,
  })
  Ciudad_idCiudad: number;

  @property({
    type: 'number',
    required: true,
  })
  TipoDocumento_idTipoDocumento: number;


  constructor(data?: Partial<Persona>) {
    super(data);
  }
}

export interface PersonaRelations {
  // describe navigational properties here
}

export type PersonaWithRelations = Persona & PersonaRelations;
