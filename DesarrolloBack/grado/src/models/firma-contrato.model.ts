import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class FirmaContrato extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  Documento: number;

  @property({
    type: 'string',
  })
  Nombre?: string;

  @property({
    type: 'string',
    required: true,
  })
  Apellidos: string;

  @property({
    type: 'string',
    required: true,
  })
  FirmaContrato: string;

  @property({
    type: 'number',
    required: true,
  })
  TipoDocumento_idTipoDocumento: number;

  @property({
    type: 'number',
    required: true,
  })
  Empresa_nitEmpresa: number;


  constructor(data?: Partial<FirmaContrato>) {
    super(data);
  }
}

export interface FirmaContratoRelations {
  // describe navigational properties here
}

export type FirmaContratoWithRelations = FirmaContrato & FirmaContratoRelations;
