import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class ContratosHasParametros extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  Contratos_idContratos: number;

  @property({
    type: 'number',
    required: true,
  })
  Parametros_idParametros: number;


  constructor(data?: Partial<ContratosHasParametros>) {
    super(data);
  }
}

export interface ContratosHasParametrosRelations {
  // describe navigational properties here
}

export type ContratosHasParametrosWithRelations = ContratosHasParametros & ContratosHasParametrosRelations;
