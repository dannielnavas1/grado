import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Contacto extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  IdContacto: number;

  @property({
    type: 'string',
    required: true,
  })
  Nombre: string;

  @property({
    type: 'string',
    required: true,
  })
  Apellido: string;

  @property({
    type: 'string',
    required: true,
  })
  Email: string;

  @property({
    type: 'number',
    required: true,
  })
  TipoDocumento_idTipoDocumento: number;

  @property({
    type: 'number',
    required: true,
  })
  Cargo_idCargo: number;

  @property({
    type: 'number',
    required: true,
  })
  Empresa_nitEmpresa: number;

  constructor(data?: Partial<Contacto>) {
    super(data);
  }
}

export interface ContactoRelations {
  // describe navigational properties here
}

export type ContactoWithRelations = Contacto & ContactoRelations;
