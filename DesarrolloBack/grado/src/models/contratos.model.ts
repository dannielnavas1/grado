import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Contratos extends Entity {
  @property({
    type: 'number',
    id: true
  })
  idContratos: number;

  @property({
    type: 'date',
    required: true,
  })
  FechaInicio: string;

  @property({
    type: 'date',
  })
  FechaFinal: string;

  @property({
    type: 'date',
    required: true,
  })
  FechaContrato: string;

  @property({
    type: 'number',
    required: true,
  })
  Salario: number;

  @property({
    type: 'boolean',
    required: true,
  })
  firmaContratoContratado: boolean

  @property({
    type: 'number',
    required: true,
  })
  TipoContrato_idTipoContrato: number;

  @property({
    type: 'number',
  })
  FirmaContrato_Documento: number;

  @property({
    type: 'number',
    required: true,
  })
  Empresa_nitEmpresa: number;

  @property({
    type: 'number',
    required: true,
  })
  Persona_idPersona: number;

  @property({
    type: 'number',
    required: true,
  })
  EstadoContrato_idEstadoContrato: number;

  @property({
    type: 'number',
    required: true,
  })
  Profesion_idProfesion: number;

  constructor(data?: Partial<Contratos>) {
    super(data);
  }
}

export interface ContratosRelations {
  // describe navigational properties here
}

export type ContratosWithRelations = Contratos & ContratosRelations;
