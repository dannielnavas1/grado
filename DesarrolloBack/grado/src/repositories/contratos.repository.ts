import {DefaultCrudRepository} from '@loopback/repository';
import {Contratos, ContratosRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContratosRepository extends DefaultCrudRepository<
  Contratos,
  typeof Contratos.prototype.idContratos,
  ContratosRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(Contratos, dataSource);
  }
}
