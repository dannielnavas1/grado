import {DefaultCrudRepository} from '@loopback/repository';
import {ContratosHasParametros, ContratosHasParametrosRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContratosHasParametrosRepository extends DefaultCrudRepository<
  ContratosHasParametros,
  typeof ContratosHasParametros.prototype.Contratos_idContratos,
  ContratosHasParametrosRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(ContratosHasParametros, dataSource);
  }
}
