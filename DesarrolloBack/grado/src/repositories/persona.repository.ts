import {DefaultCrudRepository} from '@loopback/repository';
import {Persona, PersonaRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PersonaRepository extends DefaultCrudRepository<
  Persona,
  typeof Persona.prototype.idPersona,
  PersonaRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(Persona, dataSource);
  }
}
