import {DefaultCrudRepository} from '@loopback/repository';
import {Empresa, EmpresaRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EmpresaRepository extends DefaultCrudRepository<
  Empresa,
  typeof Empresa.prototype.nitEmpresa,
  EmpresaRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(Empresa, dataSource);
  }
}
