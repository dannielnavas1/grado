import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { Contratos } from '../models';
import { ContratosRepository } from '../repositories';

class contratoSearch {
  Persona_idPersona: number;
  EstadoContrato_idEstadoContrato: number;
}


export class ContratosController {
  constructor(
    @repository(ContratosRepository)
    public contratosRepository: ContratosRepository,
  ) { }

  @post('/contratos', {
    responses: {
      '200': {
        description: 'Contratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contratos) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contratos, {
            exclude: ['idContratos'],
          }),
        },
      },
    })
    contratos: Omit<Contratos, 'idContratos'>,
  ): Promise<Contratos> {
    return this.contratosRepository.create(contratos);
  }

  @get('/contratos/count', {
    responses: {
      '200': {
        description: 'Contratos model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Contratos)) where?: Where<Contratos>,
  ): Promise<Count> {
    return this.contratosRepository.count(where);
  }

  @get('/contratos', {
    responses: {
      '200': {
        description: 'Array of Contratos model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Contratos) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Contratos)) filter?: Filter<Contratos>,
  ): Promise<Contratos[]> {
    return this.contratosRepository.find(filter);
  }

  @patch('/contratos', {
    responses: {
      '200': {
        description: 'Contratos PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contratos, { partial: true }),
        },
      },
    })
    contratos: Contratos,
    @param.query.object('where', getWhereSchemaFor(Contratos)) where?: Where<Contratos>,
  ): Promise<Count> {
    return this.contratosRepository.updateAll(contratos, where);
  }

  @get('/contratos/{id}', {
    responses: {
      '200': {
        description: 'Contratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contratos) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Contratos> {
    return this.contratosRepository.findById(id);
  }

  @get('/api/contratos/{id}', {
    responses: {
      '200': {
        description: 'Contratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contratos) } },
      },
    },
  })
  async findByIdMyContracts(@param.path.number('id') id: number): Promise<object> {
    const misContratos = await this.contratosRepository.find({ 'where': { 'Persona_idPersona': id } });
    console.info(misContratos);
    if (!misContratos) {
      console.warn(misContratos);
      console.error('No se encontraron registros asociados');
      throw new HttpErrors.NotFound('No se encuentra registrados')
    }
    console.info('La consulta trae datos');
    return misContratos;
  }

  @get('/api/contratosempresa/{id}', {
    responses: {
      '200': {
        description: 'Contratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contratos) } },
      },
    },
  })
  async findByIdMyContractsEmpresa(@param.path.number('id') id: number): Promise<object> {
    const contratosEmpresa = await this.contratosRepository.find({ 'where': { 'Empresa_nitEmpresa': id } });
    console.info(contratosEmpresa);
    if (!contratosEmpresa) {
      console.warn(contratosEmpresa);
      console.error('No se encontraron registros asociados');
      throw new HttpErrors.NotFound('No se encuentra registrados')
    }
    console.info('La consulta trae datos');
    return contratosEmpresa;
  }

  @post('/api/miscontratos', {
    responses: {
      '200': {
        description: 'Contratos model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contratos) } },
      },
    },
  })
  async buscar(@requestBody() contrato: contratoSearch): Promise<object> {
    const contrats = await this.contratosRepository.find({ 'where': { 'Persona_idPersona': contrato.Persona_idPersona, 'EstadoContrato_idEstadoContrato': contrato.EstadoContrato_idEstadoContrato } })
    if (!contrats) {
      throw new HttpErrors.NotFound('No se encuentra registrado en Enlace')
    }
    return contrats;
  }

  @patch('/contratos/{id}', {
    responses: {
      '204': {
        description: 'Contratos PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contratos, { partial: true }),
        },
      },
    })
    contratos: Contratos,
  ): Promise<void> {
    await this.contratosRepository.updateById(id, contratos);
  }

  @put('/contratos/{id}', {
    responses: {
      '204': {
        description: 'Contratos PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() contratos: Contratos,
  ): Promise<void> {
    await this.contratosRepository.replaceById(id, contratos);
  }

  @del('/contratos/{id}', {
    responses: {
      '204': {
        description: 'Contratos DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contratosRepository.deleteById(id);
  }
}
