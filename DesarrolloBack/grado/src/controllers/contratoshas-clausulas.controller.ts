import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { ContratosHasClausulas } from '../models';
import { ContratosHasClausulasRepository } from '../repositories';

export class ContratoshasClausulasController {
  constructor(
    @repository(ContratosHasClausulasRepository)
    public contratosHasClausulasRepository: ContratosHasClausulasRepository,
  ) { }

  @post('/contratosclausulas', {
    responses: {
      '200': {
        description: 'ContratosHasClausulas model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ContratosHasClausulas) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasClausulas),
        },
      },
    })
    contratosHasClausulas: Omit<ContratosHasClausulas, 'Contratos_idContratos'>,
  ): Promise<ContratosHasClausulas> {
    return this.contratosHasClausulasRepository.create(contratosHasClausulas);
  }

  @get('/contratosclausulas/count', {
    responses: {
      '200': {
        description: 'ContratosHasClausulas model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(ContratosHasClausulas)) where?: Where<ContratosHasClausulas>,
  ): Promise<Count> {
    return this.contratosHasClausulasRepository.count(where);
  }

  @get('/contratosclausulas', {
    responses: {
      '200': {
        description: 'Array of ContratosHasClausulas model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(ContratosHasClausulas) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(ContratosHasClausulas)) filter?: Filter<ContratosHasClausulas>,
  ): Promise<ContratosHasClausulas[]> {
    return this.contratosHasClausulasRepository.find(filter);
  }

  @patch('/contratosclausulas', {
    responses: {
      '200': {
        description: 'ContratosHasClausulas PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasClausulas, { partial: true }),
        },
      },
    })
    contratosHasClausulas: ContratosHasClausulas,
    @param.query.object('where', getWhereSchemaFor(ContratosHasClausulas)) where?: Where<ContratosHasClausulas>,
  ): Promise<Count> {
    return this.contratosHasClausulasRepository.updateAll(contratosHasClausulas, where);
  }

  @get('/contratosclausulas/{id}', {
    responses: {
      '200': {
        description: 'ContratosHasClausulas model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ContratosHasClausulas) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<ContratosHasClausulas> {
    return this.contratosHasClausulasRepository.findById(id);
  }

  @patch('/contratosclausulas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasClausulas PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasClausulas, { partial: true }),
        },
      },
    })
    contratosHasClausulas: ContratosHasClausulas,
  ): Promise<void> {
    await this.contratosHasClausulasRepository.updateById(id, contratosHasClausulas);
  }

  @put('/contratosclausulas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasClausulas PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() contratosHasClausulas: ContratosHasClausulas,
  ): Promise<void> {
    await this.contratosHasClausulasRepository.replaceById(id, contratosHasClausulas);
  }

  @del('/contratosclausulas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasClausulas DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contratosHasClausulasRepository.deleteById(id);
  }
}
