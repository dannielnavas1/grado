const User = require('../entities/auth.entities')
const crypto = require('crypto');
const env = require('dotenv')
const atob = require('atob');
env.config()
const secret = process.env.PASS_DANNY

const mail = require('../function/mail.funtions')
const msm = require('../function/sms.functions')

const authModel = {}

authModel.auth = async (userData) => {
  console.log(userData);
  let pass = Buffer.from(userData.Password).toLocaleString('base64')
  let hash = crypto.createHmac('sha256', secret)
    .update(pass)
    .digest('hex');
  return await User.findAll({
    where: {
      Email: userData.Email,
      Password: hash
    }
  }).then(data => {
    if (data.length === 0) {
      return {
        data,
        mensaje: 'Por favor rectifiqué los datos ingresados.'
      }
    } else {
      return {
        data,
        mensaje: 'Consulta realizada con exito'
      }
    }
  }).catch(err => {
    return {
      error: err,
      mensaje: 'No se puede procesar la solicitud'
    }
  })
}


authModel.createUser = async userData => {
  return await User.findAll({
    where: {
      'Email': userData.Email
    }
  }).then(data => {
    console.log(data);
    if (data.length === 0) {
      let pass = Buffer.from(userData.Password).toLocaleString('base64');
    let hash = crypto.createHmac('sha256', secret)
      .update(pass)
      .digest('hex');
    let jsonUser = {
      'Email': userData.Email,
      'Name': userData.Name,
      'Celular': userData.Celular,
      'Password': hash,
      'Roles_idRoles': userData.Roles_idRoles,
      'TipoDocumento_idTipoDocumento': userData.TipoDocumento_idTipoDocumento
    };
    return User.create(jsonUser).then(data => {
      console.log(data)
      let returnMail = mail.sendMail(userData.Email, 'registro', null);
      let returnMSM = msm.sendMSM(userData);
      console.log(returnMSM)
      return {
        data,
        returnMail,
        returnMSM,
        mensaje: 'Creo correctamente el usuario'
      }
    }).catch(err => {
      return {
        error: err,
        mensaje: 'No se puede procesar la solicitud'
      }
    })
    } else {
      return {
        mensaje: 'Su Correo ya se encuentra registrado'
      }
    }
  }).catch(err => {
    return {
      error: err,
      mensaje: 'Su Correo ya se encuentra registrado'
    }
  })

  
}

authModel.recoveryPassword = async userData => {
  return await User.findAll({
    where: {
      'Email': userData.Email
    }
  }).then(data => {
    console.log(data)
    if (data.length === 0) {
      return {
        result: false,
        mensaje: 'No se encuentra un usuario con el correo ingresado, por favor valide.'
      }
    } else {
      let key
      let returnMail
      let keydata = JSON.stringify(data)
      keydata = JSON.parse(keydata)
      for (let k of keydata) {
        key =  Buffer.from(k.Password + 'danny' + k.Email).toLocaleString('base64')
        returnMail = mail.sendMail(k.Email, 'Recuperar', key);
      }
      return {
        data,
        result: true,
        returnMail,
        mensaje: 'Se ha envíado un correo a la cuenta asociada.'
      }
    }
  }).catch(err => {
    return {
      err,
      result: false,
      mensaje: 'No se puede realizar la consulta'
    }
  });
}

authModel.changePassword = async userData => {
  try{
    console.log(userData)
    let arryKey = []
    let key = atob(userData.key)
    arryKey = key.split('danny')
    console.log(arryKey)
    let pass = Buffer.from(userData.userData.Password).toLocaleString('base64');
    let hash = crypto.createHmac('sha256', secret)
      .update(pass)
      .digest('hex');
    const data = await User.update(
      { Password: hash },
      { where: { 
        Email: arryKey[1],
        Password: arryKey[0]
      }
    })
    for (let d of data) {
      if(d === 1){
        let returnMail = mail.sendMail(arryKey[1], 'Change', null);
        return {
          data,
          returnMail,
          mensaje: 'Se ha envíado un correo a la cuenta asociada.'
        }
      } else {
        return {
          data,   
          mensaje: 'Se ha envíado un correo a la cuenta asociada.'
        }
      }
    }
  }catch (err) {
    return {
      err,
      mensaje: 'No se puede realizar la consulta'
    }
  }
  }

module.exports = authModel