const Sequelize = require('sequelize')
const sequelize = require('../config/settings.config')

const User = sequelize.define('Users', {
    idUsers: {type: Sequelize.SMALLINT, primaryKey: true},
    Email: Sequelize.STRING,
    Name: Sequelize.STRING,
    Password: Sequelize.STRING,
    Celular: Sequelize.STRING,
    TipoDocumento_idTipoDocumento: Sequelize.INTEGER,
    Roles_idRoles: Sequelize.INTEGER
},
{
    timestamps: false
})

module.exports = User