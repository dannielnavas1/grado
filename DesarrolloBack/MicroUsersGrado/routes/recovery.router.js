var express = require('express');
var router = express.Router();
const jwt = require('express-jwt')
const env = require('dotenv')
env.config();

const secret = { secret: process.env.PASS_SECURITY_API }
const users = require('../controllers/auth.controller')


router.post('/', (req, res)=>{
  let userData = { ...req.body }
  users.recoveryPassword(userData).then(data => {
    console.log(data);
    if (!data.result) {
      res.status(403).jsonp(data)      
    } else {
      res.status(200).jsonp(data)
    }
    }).catch(err=>{
      res.status(501).jsonp(err)
    })
})

router.put('/', async (req, res) => {
  let userData = { ...req.body }
  console.log('HOla danny')
  console.log(userData)
  const result = await users.changePassword(userData)
  for (let d of result.data) {
    if (d === 1){
      res.status(200).jsonp(result)
    } else {
      res.status(403).jsonp(result)
    }
  }
})

module.exports = router