var express = require('express');
var router = express.Router();
const jwt = require('express-jwt')
const env = require('dotenv')
env.config();

const secret = { secret: process.env.PASS_SECURITY_API }
const users = require('../controllers/auth.controller')


router.post('/', (req, res)=>{
  let userData = { ...req.body }
  users.auth(userData).then(data => {
    console.log(data)
    if (data.data.length === 0 ) {
      res.status(403).jsonp(data)      
    } else {
      res.status(200).jsonp(data)
    }
    }).catch(err=>{
      res.status(501).jsonp(err)
    })
})

module.exports = router