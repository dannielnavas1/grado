/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

function Help(props) {
  const {config: siteConfig, language = ''} = props;
  const {baseUrl, docsUrl} = siteConfig;
  const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
  const langPart = `${language ? `${language}/` : ''}`;
  const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

  const supportLinks = [
    // Learn more using the [documentation on this site.](${docUrl(
    //   'doc1.html',
    // )})
    {
      content: `Sección de ayuda de consumo de servicios en el proyecto Contratos a tu mano`,
      title: 'Ayuda APIs',
    },
    {
      content: 'Sección de ayuda de configuración del  frontend',
      title: 'Ayuda frontend',
    },
    // {
    //   content: "Find out what's new with this project",
    //   title: 'Stay up to date',
    // },
  ];

  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer documentContainer postContainer">
        <div className="post">
          <header className="postHeader">
            <h1>Necesita ayuda?</h1>
          </header>
          <p>Este proyecto esta enfocado en generar contratos de forma online.</p>
          <GridBlock contents={supportLinks} layout="threeColumn" />
        </div>
      </Container>
    </div>
  );
}

module.exports = Help;
