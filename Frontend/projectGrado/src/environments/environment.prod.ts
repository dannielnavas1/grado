export const environment = {
  production: true,
  // url: '//micro.dannielnavas.com.co/',
  // urlProject: '//app.dannielnavas.com.co/',
  // urlParametros: '//fw.dannielnavas.com.co/',
  url: '//127.0.0.1:3012/',
  urlProject: '//127.0.0.1:3011/',
  urlParametros: '//127.0.0.1:3010/',
};
