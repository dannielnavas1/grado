export class Contratos {
    FechaInicio: Date;
    FechaFinal: Date;
    FechaContrato: Date;
    TipoContrato_idTipoContrato: number;
    firmaContratoContratado: boolean;
    FirmaContrato_Documento: number;
    Empresa_nitEmpresa: number;
    Persona_idPersona: number;
    EstadoContrato_idEstadoContrato: number;
    Salario: number;
    Profesion_idProfesion: number;
}
