import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import { SearchMyContracts } from './../../../models/zonaPrivada/search-my-contracts';
import { Component, OnInit } from '@angular/core';
import { ContratosService } from 'src/app/service/personas/contratos/contratos.service';
import { ActivatedRoute, Router } from '@angular/router'
import { UserCountService } from 'src/app/service/utils/user-count.service';
// import swal from 'sweetalert';
// const Swal = require('sweetalert2')

@Component({
  selector: 'app-lista-contratos',
  templateUrl: './lista-contratos.component.html',
  styleUrls: ['./lista-contratos.component.less']
})
export class ListaContratosComponent implements OnInit {
  myContracts: SearchMyContracts = new SearchMyContracts();
  user: any;
  misContratos: any;
  empresa: any;
  contratosEmpresa: any;
  dataEmpresa: any;

  constructor(private contratosService: ContratosService,
              private naturalService: NaturalService,
              private userCountService: UserCountService,
              private _route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.user = this.userCountService.authCount();
    if(this._route.snapshot.paramMap.get('idContrato') === '1' || this._route.snapshot.paramMap.get('idContrato') === '2') {
      this.myContracts.Persona_idPersona = this.user.idUsers;
      this.myContracts.EstadoContrato_idEstadoContrato = parseInt(this._route.snapshot.paramMap.get('idContrato'));
      this.contratosService.getMyContracts(this.myContracts).subscribe(data => {
        console.log(data);
        this.misContratos = data;
      }, err => {
        console.log(err);
      });
    } else if (this._route.snapshot.paramMap.get('idContrato') === '4') {
      this.contratosService.getContractosEmpresa(this.user.idUsers).subscribe(data => {
        console.log('Contratos empresa ', data)
        this.misContratos = data;
      }, err => {
        console.log(err);
      });
    } else {
      this.contratosService.getMisContratos(this.user.idUsers).subscribe(data => {
        this.misContratos = data;
        console.log(this.misContratos);
      }, err => {
        console.log(err);
      })
    }
  }

  verDatosEmpresa(idEmpresa){
    this.naturalService.getPersonaJuridica(idEmpresa).subscribe(data => {
      this.dataEmpresa = data;
      console.log(this.dataEmpresa)
      alert(`Nombre de la empresa: ${ this.dataEmpresa.Nombre }
               Dirección: ${ this.dataEmpresa.Direccion }
               Pagina web: ${ this.dataEmpresa.Pagina }`)
      // Swal({
      //   title: "Información de la empresa!",
      //   text: `
      //         Nombre de la empresa: ${ this.dataEmpresa.Nombre }
      //         Dirección: ${ this.dataEmpresa.Direccion }
      //         Pagina web: ${ this.dataEmpresa.Pagina }
      //         `
      // });
    })
  }

  irContrato(idContratos){
    this.router.navigate(['micontrato/'  + idContratos])
  }

}
