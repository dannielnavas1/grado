import { CookieService } from 'ngx-cookie-service';
import { LoginService } from 'src/app/service/zonaPublica/login.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.less']
})
export class ContratosComponent implements OnInit {
  persona: FormGroup;
  
  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private cookie: CookieService) { }

  ngOnInit() {
    this.persona = this.fb.group({
      idPersona: ['', [Validators.required,   Validators.pattern(/^[0-9\s]+$/)]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Apellido: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Direccion: ['', [Validators.required]],
      firmaContratoContratado: [false],
      FirmaContratoPersona: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]]
    });
  }

  onSubmit(){
    console.log(this.persona.value);
  }

}
