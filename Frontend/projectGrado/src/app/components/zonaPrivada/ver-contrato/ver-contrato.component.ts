import { CookieService } from 'ngx-cookie-service';
import { UserCountService } from './../../../service/utils/user-count.service';
import { ContratosService } from './../../../service/personas/contratos/contratos.service';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contratos } from 'src/app/models/zonaPrivada/contratos';

@Component({
  selector: 'app-ver-contrato',
  templateUrl: './ver-contrato.component.html',
  styleUrls: ['./ver-contrato.component.less']
})
export class VerContratoComponent implements OnInit {
  idContrato = this._route.snapshot.paramMap.get('idContrato');
  documento: any;
  miContrato: any;
  userCount: any;
  contrato: Contratos;
  miContratoPermiso: string;
  clausula: any;
  fechaContrato: Date;
  prof: any;

  constructor(private _route: ActivatedRoute,
              private contratosService: ContratosService,
              private cookie: CookieService,
              private userCountService: UserCountService,
              private parametrosService: ParametrosService) { }

  ngOnInit() {
    this.miContratoPermiso = this.cookie.get('tipoPerfil');
    this.userCount = this.userCountService.authCount();
    this.contratosService.getMiContrato(this.idContrato).subscribe(data => {
      this.contrato = data;
      this.fechaContrato = this.contrato.FechaInicio;
      console.log(data);
      this.miContrato = data; 
      this.parametrosService.getIdProfesiones(this.miContrato.Profesion_idProfesion).subscribe(data => this.prof = data )

      this.parametrosService.getAlmacenContratosId(this.miContrato.TipoContrato_idTipoContrato).subscribe(data => {
        this.documento = data
        for (let cont of this.documento) {
          this.documento = cont.AlmacenContratos.replace('${fechaContrato}', this.fechaContrato)
          this.documento = this.documento.replace('${cargo}', this.prof.Profesion)
        }
      })
      this.contratosService.getHasContratoClausula(this.idContrato).subscribe(data=>{
        console.log('Mis Clausulas', data)
        this.parametrosService.getClausulasId(data.Clausulas_idClausulas).subscribe(data => {
          console.log(data)
          this.clausula = data
        }, err => {
          console.log(err)
        })
      })

    })
  }

  firmarContrato() {
    this.contrato.firmaContratoContratado = true;
    this.contrato.EstadoContrato_idEstadoContrato = 2;
    this.contratosService.putContrato(this.idContrato, this.contrato).subscribe(data => {
      console.log(data);
    }, err => {
      console.log(err);
    })
  }

}
