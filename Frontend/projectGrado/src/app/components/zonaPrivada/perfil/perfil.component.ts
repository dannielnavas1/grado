import { LoginService } from 'src/app/service/zonaPublica/login.service';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.less']
})
export class PerfilComponent implements OnInit {
  persona: FormGroup;
  empresas: FormGroup;
  user: any;

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private cookie: CookieService) { }

  ngOnInit() {
    this.user = JSON.parse(this.cookie.get('userAcount'));
    this.persona = this.fb.group({
      idPersona: ['', [Validators.required]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Apellido: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Direccion: ['', [Validators.required]],
      FirmaContratoPersona: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]]
    });
    this.empresas = this.fb.group({
      nitEmpresa: ['', [Validators.required]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      numeroEmpleados: ['', [Validators.required]],
      Direccion: ['', [Validators.required]],
      Pagina: ['', [Validators.required, Validators.pattern(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)]],
      Sector_idSector: ['', [Validators.required]],
      Tipologia_idTipologia: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]]
    });

    this.loginService.getDataUser(this.user.idUsers).subscribe(data => {
      this.persona.controls['idPersona'].setValue(data.idPersona);
      this.persona.controls['Nombre'].setValue(data.Nombre);
      this.persona.controls['Apellido'].setValue(data.Apellido);
      this.persona.controls['Direccion'].setValue(data.Direccion);
      this.persona.controls['FirmaContratoPersona'].setValue(data.FirmaContratoPersona);
      this.persona.controls['Profesion_idProfesion'].setValue(data.Profesion_idProfesion);
      this.persona.controls['Pais_idPais'].setValue(data.Pais_idPais);
      this.persona.controls['Departamento_idDepartamento'].setValue(data.Departamento_idDepartamento);
      this.persona.controls['Ciudad_idCiudad'].setValue(data.Ciudad_idCiudad);
      this.persona.controls['TipoDocumento_idTipoDocumento'].setValue(data.TipoDocumento_idTipoDocumento);
    }, error => {
      this.loginService.getDataEmpresa(this.user.idUsers)
        .subscribe(data => {
          this.empresas.controls['nitEmpresa'].setValue(data.nitEmpresa);
          this.empresas.controls['Nombre'].setValue(data.Nombre);
          this.empresas.controls['numeroEmpleados'].setValue(data.numeroEmpleados);
          this.empresas.controls['Direccion'].setValue(data.Direccion);
          this.empresas.controls['Pagina'].setValue(data.Pagina);
          this.empresas.controls['Sector_idSector'].setValue(data.Sector_idSector);
          this.empresas.controls['Tipologia_idTipologia'].setValue(data.Tipologia_idTipologia);
          this.empresas.controls['Pais_idPais'].setValue(data.Pais_idPais);
          this.empresas.controls['Departamento_idDepartamento'].setValue(data.Departamento_idDepartamento);
          this.empresas.controls['Ciudad_idCiudad'].setValue(data.Ciudad_idCiudad);
        }, err => {

        });
    });
  }

  onSubmit(){
    console.log(this.persona.value);
  }

  onSubmitJuridica(){
    console.log(this.empresas.value);
  }

}
