import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-documento',
  templateUrl: './tipo-documento.component.html',
  styleUrls: ['./tipo-documento.component.less']
})
export class TipoDocumentoComponent implements OnInit {
  tiposDocumentos: void;
  documentos: FormGroup

  constructor(private parametrosService: ParametrosService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.documentos = this.fb.group({
      TipoDocumento: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]]
    })
    this.parametrosService.getTipoDocumento().subscribe(data => this.tiposDocumentos = data)
  }

  onSubmit() {
    if(this.documentos.valid){
      this.parametrosService.postTipoDocumento(this.documentos.value).subscribe(data => {
        console.log(data)
      })
    }
  }

  delete(doc){
    this.parametrosService.deleteTipoDocumento(doc.idTipoDocumento).subscribe(data => {
      console.log(data)
    })
  }

}
