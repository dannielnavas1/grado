import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClausulasComponent } from './clausulas.component';

describe('ClausulasComponent', () => {
  let component: ClausulasComponent;
  let fixture: ComponentFixture<ClausulasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClausulasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClausulasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
