import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clausulas',
  templateUrl: './clausulas.component.html',
  styleUrls: ['./clausulas.component.less']
})
export class ClausulasComponent implements OnInit {
  clausulaForm: FormGroup;
  clausulas: any;

  constructor(private parametrosService: ParametrosService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.parametrosService.getClausulas().subscribe(data => this.clausulas = data)
    this.clausulaForm = this.fb.group({
      TipoClausula: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Clausulas: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]]
    });
  }

  onSubmit() {
    if(this.clausulaForm.valid){
      this.parametrosService.postClausualas(this.clausulaForm.value).subscribe(data => {
        console.log(data);
      })
    }
  }

  delete(clausula) {
    this.parametrosService.deleteClausualas(clausula.idClausulas).subscribe(data => {
      console.log(data);
    })
  }

}
