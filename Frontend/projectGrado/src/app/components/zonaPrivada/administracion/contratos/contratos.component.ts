import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contratosadmin',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.less']
})
export class ContratosAdminComponent implements OnInit {
  tiposContrato: any;
  tiposContratos: FormGroup;
  formulario: boolean;
  contrato: FormGroup;

  constructor(private parametrosService: ParametrosService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.parametrosService.getTipoContratos().subscribe(data => this.tiposContrato = data );
    this.tiposContratos = this.fb.group({
      TipoContrato: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Descripcion: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]]
    });
    this.contrato = this.fb.group({
      AlmacenContratos: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      TipoContrato_idTipoContrato: ['']
    })
  }

  onSubmit() {
    if(this.tiposContratos.valid){
      this.parametrosService.postTipoContrato(this.tiposContratos.value).subscribe(data => {
        console.log(data)
        this.contrato.controls['TipoContrato_idTipoContrato'].setValue(data.idTipoContrato);
        this.formulario = true;
      });
    }
  }

  onSubmitContratos() {
    if(this.contrato.valid){
      this.parametrosService.postAlmacenContratos(this.contrato.value).subscribe(data => {
        console.log(data);
      })
    }
  }

  delete(tipoContrato) {
    this.parametrosService.deleteTipoContrato(tipoContrato.idTipoContrato).subscribe(data => {
      console.log(data);
    })
  }

}
