import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratosAdminComponent } from './contratos.component';

describe('ContratosComponent', () => {
  let component: ContratosAdminComponent;
  let fixture: ComponentFixture<ContratosAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratosAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratosAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
