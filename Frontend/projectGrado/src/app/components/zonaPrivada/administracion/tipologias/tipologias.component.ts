import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipologias',
  templateUrl: './tipologias.component.html',
  styleUrls: ['./tipologias.component.less']
})
export class TipologiasComponent implements OnInit {
  tipologias: any;
  tipologia: FormGroup;

  constructor(private parametrosService: ParametrosService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.tipologia = this.fb.group({
      Tipologia: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]]
    })
    this.parametrosService.getTipologia().subscribe(data => this.tipologias = data)
  }

  onSubmit() {
    if(this.tipologia.valid){
      this.parametrosService.postTipologia(this.tipologia.value).subscribe(data => console.log(data));
    }
  }

  delete(tipologia) {
    this.parametrosService.deleteTipologia(tipologia.idTipologia).subscribe(data => {
      console.log(data);
    })
  }

}
