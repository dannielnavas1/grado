import { SearchMyContracts } from './../../../models/zonaPrivada/search-my-contracts';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { ContratosService } from 'src/app/service/personas/contratos/contratos.service';
import { Contratos } from 'src/app/models/zonaPrivada/contratos';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less']
})
export class PanelComponent implements OnInit {
  showFiller = true;
  userLogin: any;
  infoUserLoged: any;
  profesion: any;
  contratosCuentas: any;
  countPendientes: any;
  pendientes = [];
  finalizado = [];
  pendientesCounter: number;
  finalizadoCounter: number;
  misContratos: any;
  myContracts: SearchMyContracts = new SearchMyContracts();
  personaNatural: boolean = false;
  personaJuridica: boolean;
  contratosEmpresa: any;
  

  constructor(private cookie: CookieService,
              private naturaService: NaturalService,
              private contratosService: ContratosService,
              private parametrosServices: ParametrosService) {
  }

  ngOnInit() {
    this.userLogin = JSON.parse(this.cookie.get('userAcount'));
    console.log(this.userLogin);
    // idUsers
    this.naturaService.getPersonaNatural(this.userLogin.idUsers)
      .subscribe(data => {
        this.personaNatural = true;
        this.cookie.set('tipoPerfil', 'Natural');
        this.infoUserLoged = data;
        console.log('Informacion', this.infoUserLoged)
        this.parametrosServices.getIdProfesiones(this.infoUserLoged.Profesion_idProfesion)
          .subscribe(data => {
            this.profesion = data;
            this.cookie.set('Profesion', this.profesion.Profesion);
            console.log(this.profesion)
          }, err => {
            alert('No podemos obtener su profesión');
          });
      }, err => {
        this.naturaService.getPersonaJuridica(this.userLogin.idUsers)
          .subscribe(data => {
            this.infoUserLoged = data;
            this.personaJuridica = true;
            this.cookie.set('tipoPerfil', 'Empresa');
            console.log(this.infoUserLoged)
          }, err => {
            console.log(err);
            alert('No podemos traer sus datos');
          });
      });
    this.myContracts.Persona_idPersona = this.userLogin.idUsers;
      // getContractosEmpresa
    console.log(this.personaNatural);
    // if (this.personaNatural) {
    this.myContracts.EstadoContrato_idEstadoContrato = 1;
    this.contratosService.getMyContracts(this.myContracts).subscribe(data => {
      console.log(data)
      this.pendientesCounter = data.length;
    }, err => {
      this.pendientesCounter = 0;
    })
    this.myContracts.EstadoContrato_idEstadoContrato = 2;
    this.contratosService.getMyContracts(this.myContracts).subscribe(data => {
      // console.log(data)
      this.finalizadoCounter = data.length;
    }, err => {
      this.finalizadoCounter = 0;
    })
    this.contratosService.getMisContratos(this.userLogin.idUsers).subscribe(data => {
      // console.log(data)
      this.misContratos = data.length;
    }, err => {
      this.misContratos =  0;
      console.log(err)
    });
    // } 
  }
}
