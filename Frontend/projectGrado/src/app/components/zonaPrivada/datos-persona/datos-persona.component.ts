import { UserCountService } from 'src/app/service/utils/user-count.service';
import { ParametrosService } from './../../../service/parametros/parametros.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import { Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';

@Component({
  selector: 'app-datos-persona',
  templateUrl: './datos-persona.component.html',
  styleUrls: ['./datos-persona.component.less']
})
export class DatosPersonaComponent implements OnInit {
  persona: FormGroup;
  empresas: FormGroup;
  contactos: FormGroup;
  firmanteForm: FormGroup;
  type: boolean = true;
  natural: boolean = false;
  juridica: boolean = false;
  contacto: boolean = false;
  firmante: boolean = false;
  profeciones: any;
  pais: any;
  departamento: any;
  ciudad: any;
  tipologias: any;
  sectores: any;
  tiposDocumentos: any;
  cargos: any;
  Parametros: any;
  @ViewChild(SignaturePad, {static: false}) signaturePad: SignaturePad;
 
  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 500,
    'canvasHeight': 300
  };
  userCount: any;

  constructor(private fb: FormBuilder,
              private naturalService: NaturalService,
              private router: Router,
              private userCountService: UserCountService,
              private parametrosService: ParametrosService,
              private cookie: CookieService) {
                if (this.cookie.check('valor')) {
                  if(this.cookie.get('valor') === '1') {
                    this.natural = true;
                    this.type = false;
                  } else {
                    this.juridica = true;
                    this.type = false;
                  }
                }
                this.parametrosService.getProfesiones().subscribe(data => this.profeciones = data);
                this.parametrosService.getPais().subscribe(data => this.pais = data);
                this.parametrosService.getDepartamento().subscribe(data => this.departamento = data);
                this.parametrosService.getCiudad().subscribe(data => this.ciudad = data);
                this.parametrosService.getSectores().subscribe(data => this.sectores = data);
                this.parametrosService.getTipologia().subscribe(data => this.tipologias = data);
                this.parametrosService.getTipoDocumento().subscribe(data => this.tiposDocumentos = data);
                this.parametrosService.getCargos().subscribe(data => this.cargos = data);
              }

  ngOnInit() {
    this.userCount = this.userCountService.authCount();
    this.persona = this.fb.group({
      idPersona: ['', [Validators.required]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Apellido: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Direccion: ['', [Validators.required]],
      FirmaContratoPersona: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]]
    });
    this.empresas = this.fb.group({
      nitEmpresa: ['', [Validators.required]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      numeroEmpleados: ['', [Validators.required,   Validators.pattern(/^[0-9\s]+$/)]],
      Direccion: ['', [Validators.required]],
      Pagina: ['', [Validators.required, Validators.pattern(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)]],
      Sector_idSector: ['', [Validators.required]],
      Tipologia_idTipologia: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]]
    });
    this.contactos = this.fb.group({
      IdContacto: ['', [Validators.required, Validators.pattern(/^[0-9\s]+$/)]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Apellido: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/)]],
      Firmado: [false],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]],
      Cargo_idCargo: ['', [Validators.required]],
      Empresa_nitEmpresa: ['', [Validators.required]]
    });
    this.firmanteForm = this.fb.group({
      Documento: ['', [Validators.required,   Validators.pattern(/^[0-9\s]+$/)]],
      Nombre: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      Apellidos: ['', [Validators.required, Validators.pattern(/^(?!.*(.)\1{3})/), Validators.pattern(/^[A-Za-z\s]+$/),  Validators.pattern(/^(?!\s)/)]],
      FirmaContrato: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]],
      Empresa_nitEmpresa: ['', [Validators.required]]
    });
    const data = JSON.parse(this.cookie.get('userAcount'));
    if (!this.natural) {
      this.empresas.controls['nitEmpresa'].setValue(data.idUsers);
      this.firmanteForm.controls['Empresa_nitEmpresa'].setValue(data.idUsers);
      this.contactos.controls['Empresa_nitEmpresa'].setValue(data.idUsers);
      // this.empresas.controls['TipoDocumento_idTipoDocumento'].setValue(data.TipoDocumento_idTipoDocumento);
    } else {
      this.persona.controls['idPersona'].setValue(data.idUsers);
      this.persona.controls['TipoDocumento_idTipoDocumento'].setValue(data.TipoDocumento_idTipoDocumento);
    }
  }

  registerPerson(event) {
    this.cookie.set('valor', event);
    if (event === 1) {
      this.natural = true;
      this.type = false;
    } else {
      this.juridica = true;
      this.type = false;
    }
  }

  onSubmit() {
    this.persona.controls['idPersona'].setValue(this.userCount.idUsers)
    this.persona.controls['TipoDocumento_idTipoDocumento'].setValue(this.userCount.TipoDocumento_idTipoDocumento)
    this.persona.controls['FirmaContratoPersona'].setValue(localStorage.getItem('firma'));
    this.naturalService.postPersonaNatural(this.persona.value)
      .subscribe(data => {
        this.router.navigate(['/panel']);
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitJuridica() {
    this.naturalService.postPersonaJuridica(this.empresas.value)
      .subscribe(data => {
        this.juridica = false;
        this.contacto = true;
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitContacto() {
    this.naturalService.postPersonaContacto(this.contactos.value)
      .subscribe(data => {
        this.contacto = false;
        this.firmante = true;
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitFirmante() {
    this.firmanteForm.controls['FirmaContrato'].setValue(localStorage.getItem('firma'));
    this.naturalService.postPersonaFirmante(this.firmanteForm.value)
      .subscribe(data => {
        this.router.navigate(['/panel'])
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  clearFirma(event) {
    this.signaturePad.clear();
    event.preventDefault();
  }

  ngAfterViewInit() {
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }
 
  drawComplete() {
    // will be notified of szimek/signature_pad's onEnd event
    console.log(this.signaturePad.toDataURL());
  }

  saveFirma(event) {
    localStorage.setItem('firma', this.signaturePad.toDataURL());
    if (event.preventDefault() !== undefined) {
      event.preventDefault();
    }
  }
 
  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }

}
