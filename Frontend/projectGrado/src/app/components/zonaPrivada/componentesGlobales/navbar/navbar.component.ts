import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {
  user: any;

  constructor(private cookie: CookieService,
              private router: Router) { }

  ngOnInit() {
    this.user = JSON.parse(this.cookie.get('userAcount'))
  }

  logout() {
    this.cookie.deleteAll();
    this.router.navigate(['/']);
  }

}
