import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistroService } from 'src/app/service/zonaPublica/registro.service';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.component.html',
  styleUrls: ['./recuperar.component.less']
})
export class RecuperarComponent implements OnInit {
  recuperar: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private registroService: RegistroService) { }

  ngOnInit() {
    this.recuperar = this.fb.group({
      Email: ['', [Validators.required, Validators.email, Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/)]],
    });
  }

  onSubmit() {
    if (this.recuperar.valid) {
      this.registroService.recovery(this.recuperar.value)
        .subscribe(data => {
          this.router.navigate(['/login']);
        }, err => {
          alert('No se puede registrar usuario');
        });
    }
  }

}
