import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/service/zonaPublica/login.service';

import { Auth } from '../../../models/persist/auth';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  loginActivate: any;
  datos: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private cookie: CookieService,
              private loginService: LoginService) { }

  ngOnInit() {
    this.login = this.fb.group({
      Email: ['', [Validators.email, Validators.required, Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/)]],
      Password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    if(this.login.valid) {
      this.loginService.login(this.login.value)
      .subscribe(data => {
        this.loginActivate = data;
        if (this.loginActivate.mensaje === 'Consulta realizada con exito') {
          console.log('Login realizado');
          let u = this.loginActivate;
          this.loginService.setUserLoggedIn(u);
          this.cookie.set('coopIdent', btoa('true'));
          for(let d of this.loginActivate.data) {
            this.cookie.set('userAcount', JSON.stringify(d));
            console.log(d);
            this.loginService.getDataUser(d.idUsers).subscribe(data => {
              console.log('Danniel ', data)
              this.router.navigate(['/panel']);
            }, error => {
              this.loginService.getDataEmpresa(d.idUsers)
                .subscribe(data => this.router.navigate(['/panel']), err => this.router.navigate(['/personal']))
            });
          }
          // console.log(this.cookie.get('userAcount'));
        } else {
          console.log('No podemos loguearnos');
        }
      }, err => {
        alert('No podemos realizar sesión');
      });
    }
  }

}
