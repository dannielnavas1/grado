import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperarIdComponent } from './recuperar-id.component';

describe('RecuperarIdComponent', () => {
  let component: RecuperarIdComponent;
  let fixture: ComponentFixture<RecuperarIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecuperarIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuperarIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
