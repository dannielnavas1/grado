import { TipologiasComponent } from './components/zonaPrivada/administracion/tipologias/tipologias.component';
import { TipoDocumentoComponent } from './components/zonaPrivada/administracion/tipo-documento/tipo-documento.component';
import { ClausulasComponent } from './components/zonaPrivada/administracion/clausulas/clausulas.component';
import { VerContratoComponent } from './components/zonaPrivada/ver-contrato/ver-contrato.component';
import { ContratosComponent } from './components/zonaPrivada/contratos/contratos.component';
import { ListaContratosComponent } from './components/zonaPrivada/lista-contratos/lista-contratos.component';
import { PerfilComponent } from './components/zonaPrivada/perfil/perfil.component';
import { AdminComponent } from './components/zonaPrivada/admin/admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { PrincipalComponent } from './components/zonaPublica/principal/principal.component';
import { LoginComponent } from './components/zonaPublica/login/login.component';
import { RegistroComponent } from './components/zonaPublica/registro/registro.component';
import { PanelComponent } from './components/zonaPrivada/panel/panel.component';
import { RecuperarComponent } from './components/zonaPublica/recuperar/recuperar.component';
import { RecuperarIdComponent } from './components/zonaPublica/recuperar-id/recuperar-id.component';
import { DatosPersonaComponent } from './components/zonaPrivada/datos-persona/datos-persona.component';
import { GeneradorContratoComponent } from './zonaPrivada/generador-contrato/generador-contrato.component';
import { ContratosAdminComponent } from './components/zonaPrivada/administracion/contratos/contratos.component';


const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegistroComponent
  },
  {
    path: 'recovery',
    component: RecuperarComponent
  },
  {
    path: 'change/:id',
    component: RecuperarIdComponent
  },
  {
    path: 'panel',
    component: PanelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personal',
    component: DatosPersonaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'perfil',
    component: PerfilComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'listacontratos/:idContrato',
    component: ListaContratosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contratos/:idContrato',
    component: ContratosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'generadorcontrato',
    component: GeneradorContratoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admincontractos',
    component: ContratosAdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'adminclausulas',
    component: ClausulasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admindocumento',
    component: TipoDocumentoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admintipologias',
    component: TipologiasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'micontrato/:idContrato',
    component: VerContratoComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
