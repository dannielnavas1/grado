import { ContratosAdminComponent } from './components/zonaPrivada/administracion/contratos/contratos.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { SignaturePadModule } from 'angular2-signaturepad';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './components/zonaPublica/principal/principal.component';
import { LoginComponent } from './components/zonaPublica/login/login.component';
import { RegistroComponent } from './components/zonaPublica/registro/registro.component';
import { RecuperarComponent } from './components/zonaPublica/recuperar/recuperar.component';
import { RecuperarIdComponent } from './components/zonaPublica/recuperar-id/recuperar-id.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PanelComponent } from './components/zonaPrivada/panel/panel.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { DatosPersonaComponent } from './components/zonaPrivada/datos-persona/datos-persona.component';
import { AdminComponent } from './components/zonaPrivada/admin/admin.component';
import { ContratosComponent } from './components/zonaPrivada/contratos/contratos.component';
import { PerfilComponent } from './components/zonaPrivada/perfil/perfil.component';
import { ListaContratosComponent } from './components/zonaPrivada/lista-contratos/lista-contratos.component';
import { NavbarComponent } from './components/zonaPrivada/componentesGlobales/navbar/navbar.component';
import { AbogadoComponent } from './components/zonaPrivada/abogado/abogado.component';
import { MenuComponent } from './components/zonaPrivada/componentesGlobales/menu/menu.component';
import { GeneradorContratoComponent } from './zonaPrivada/generador-contrato/generador-contrato.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { VerContratoComponent } from './components/zonaPrivada/ver-contrato/ver-contrato.component';
import { TipoDocumentoComponent } from './components/zonaPrivada/administracion/tipo-documento/tipo-documento.component';
import { TipologiasComponent } from './components/zonaPrivada/administracion/tipologias/tipologias.component';
import { ClausulasComponent } from './components/zonaPrivada/administracion/clausulas/clausulas.component';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    LoginComponent,
    RegistroComponent,
    RecuperarComponent,
    RecuperarIdComponent,
    PanelComponent,
    DatosPersonaComponent,
    AdminComponent,
    ContratosComponent,
    ContratosAdminComponent,
    PerfilComponent,
    ListaContratosComponent,
    NavbarComponent,
    AbogadoComponent,
    MenuComponent,
    GeneradorContratoComponent,
    VerContratoComponent,
    TipoDocumentoComponent,
    TipologiasComponent,
    ClausulasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    SignaturePadModule,
    SignaturePadModule
  ],
  providers: [
    CookieService,
    MatDatepickerModule,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
