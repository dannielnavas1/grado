import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneradorContratoComponent } from './generador-contrato.component';

describe('GeneradorContratoComponent', () => {
  let component: GeneradorContratoComponent;
  let fixture: ComponentFixture<GeneradorContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneradorContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneradorContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
