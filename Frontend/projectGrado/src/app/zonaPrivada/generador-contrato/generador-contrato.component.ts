import { Router } from '@angular/router';
import { Clausulas } from './../../models/zonaPrivada/clausulas';
import { CookieService } from 'ngx-cookie-service';
import { Contratos } from './../../models/zonaPrivada/contratos';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ContratosService } from 'src/app/service/personas/contratos/contratos.service';
import { UserCountService } from 'src/app/service/utils/user-count.service';
import { UserCount } from 'src/app/models/zonaPrivada/user-count';
import { Parametros } from 'src/app/models/zonaPrivada/parametros';

@Component({
  selector: 'app-generador-contrato',
  templateUrl: './generador-contrato.component.html',
  styleUrls: ['./generador-contrato.component.less']
})
export class GeneradorContratoComponent implements OnInit {
  contratro: FormGroup;
  contenedorPersonas: any;
  options;
  personaContratar: any;
  pais: any;
  depa: any;
  ciudad: any;
  viewCardPersona: boolean = false;
  almacenContratos: any;
  clausulas: any;
  contratoRegister: Contratos = new Contratos();
  userCount: UserCount = new UserCount();
  contratoClausulas: Clausulas = new Clausulas();
  contratoParametros: Parametros = new Parametros();
  Parametros: any;
  cargos: any;
  profeciones: any;

  constructor(private fb: FormBuilder,
              private parametrosService: ParametrosService,
              private contratosService: ContratosService,
              private userCountService: UserCountService,
              private router: Router,
              private naturalService: NaturalService) { }

  ngOnInit() {
    this.parametrosService.getProfesiones().subscribe(data => this.profeciones = data);
    this.contratro = this.fb.group({
      Persona_idPersona: ['', [Validators.required, ,   Validators.pattern(/^[0-9\s]+$/)]],
      FechaInicio: ['', [Validators.required]],
      FechaFinal: [''],
      FechaContrato: [''],
      firmaContratoContratado: [true],
      Salario: ['', [Validators.required, ,   Validators.pattern(/^[0-9\s]+$/)]],
      TipoContrato_idTipoContrato: ['', [Validators.required]],
      FirmaContrato_Documento: [''],
      Empresa_nitEmpresa: [''],
      // parametros: [''],
      clausulas: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
    });
    this.naturalService.getPersonaNaturalAll().subscribe(data => {
      this.options = data;
    });
    this.parametrosService.getTipoContratos().subscribe(data => {
      this.almacenContratos = data;
    });
    this.parametrosService.getClausulas().subscribe(data => {
      this.clausulas = data;
    });
    this.parametrosService.getParametros().subscribe(data => this.Parametros = data)
    this.userCount = this.userCountService.authCount();
    console.log(this.userCount)
    this.contratoRegister.Empresa_nitEmpresa = this.userCount.idUsers;
    this.contratoRegister.EstadoContrato_idEstadoContrato = 1;
    this.naturalService.getIdFirmante(this.userCount.idUsers).subscribe(data => {
      this.contratoRegister.FirmaContrato_Documento = data.Documento; 
    });
  }

  consultarPersona(event) {
    console.log(event.target.value)
    this.naturalService.getPersonaNatural(event.target.value)
      .subscribe(data => {
        this.viewCardPersona = true;
        this.personaContratar = data;
        console.log(this.personaContratar)
        this.parametrosService.getIdPais(this.personaContratar.Pais_idPais)
          .subscribe(data => this.pais = data )
        this.parametrosService.getIdCiudad(this.personaContratar.Ciudad_idCiudad)
        .subscribe(data => this.ciudad = data )
        this.parametrosService.getIdDepartamento(this.personaContratar.Departamento_idDepartamento)
          .subscribe(data => this.depa = data )
      }, err => console.log('Persona no encontrada'));
  }

  consultarClausulas(event) {
    const asignar = this.clausulas.filter(data => data.TipoClausula === event.target.value);
    for (let asig of asignar) {
      this.contratoClausulas.Clausulas_idClausulas = asig.idClausulas;
    }
  }

  consultarParametros(event){
    const asignar = this.Parametros.filter(data => data.Parametros === event.target.value);
    for (let asig of asignar) {
      this.contratoParametros.Parametros_idParametros = asig.idParametros;
    }
  }

  consultarContratos(event) {
    const asignar = this.almacenContratos.filter(data => data.TipoContrato === event.target.value);
    for (let asig of asignar) {
      this.contratoRegister.TipoContrato_idTipoContrato = asig.idTipoContrato;
    }
    // this.typeContrato
  }

  onSubmit() {
    this.contratoRegister.FechaContrato = this.contratro.value.FechaInicio;
    this.contratoRegister.FechaFinal = this.contratro.value.FechaFinal;
    this.contratoRegister.FechaInicio = this.contratro.value.FechaInicio;
    this.contratoRegister.Persona_idPersona = parseInt(this.contratro.value.Persona_idPersona);
    this.contratoRegister.Salario = this.contratro.value.Salario;
    this.contratoRegister.Profesion_idProfesion  = this.contratro.value.Profesion_idProfesion;
    this.contratoRegister.firmaContratoContratado = false;
    if(this.contratro.valid) {
      this.contratosService.postContrato(this.contratoRegister).subscribe(data => {
        console.log('Se creo el contrato Correctamente');
        console.log(data);
        this.contratoClausulas.Contratos_idContratos = data.idContratos;
        this.contratoParametros.Contratos_idContratos = data.idContratos;
        this.registrarClausulas();
        // this.registrarParametros();
        this.router.navigate(['/panel'])
      }, err => {
        console.log('No se puedo crear el contrato');
      });
    }
  }

  registrarParametros(){
    this.naturalService.postContratoParametros(this.contratoParametros).subscribe(data => {
      console.log('Danniel Navas', data);
    });
  }

  registrarClausulas(){
    this.naturalService.postContratoClausulas(this.contratoClausulas).subscribe(data => {
      console.log('Danniel Navas', data);
    });
  }

}
