import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../../models/zonaPublica/login/login';
import { environment } from 'src/environments/environment';
import { Auth } from '../../models/persist/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private isUserLoggedIn;
  public UserLogged;
  userLogget: string;

  setUserLoggedIn(user) {
    this.isUserLoggedIn = true;
    this.UserLogged = user;
    sessionStorage.setItem('currentUser', btoa(this.isUserLoggedIn.idUser));
  }

  getUserLoggedIn() {
    this.userLogget = sessionStorage.getItem('currentUser');
    return atob(this.userLogget);
  }

  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
  }

  login(data) {
    return this.http.post<Login>(environment.url + 'api/auth', data);
  }

  getDataUser(data) {
    return this.http.get<any>(environment.urlProject + '/personas/' + data);
  }
  getDataEmpresa(data) {
    return this.http.get<any>(environment.urlProject + '/empresas/' + data);
  }

}
