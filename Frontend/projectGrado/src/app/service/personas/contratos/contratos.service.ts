import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContratosService {

  constructor(private http: HttpClient) { }

  postContrato(data) {
    return this.http.post<any>(environment.urlProject + 'contratos', data);
  }

  getContrato() {
    return this.http.get<any>(environment.urlProject + 'contratos' );
  }

  getCountContrato(token) {
    return this.http.get<any>(environment.urlProject + 'contratos/' + token);
  }

  getMisContratos(token) {
    return this.http.get<any>(environment.urlProject + 'api/contratos/' + token );
  }

  getMiContrato(token) {
    return this.http.get<any>(environment.urlProject + 'contratos/' + token );
  }

  deleteContrato(token) {
    return this.http.delete<any>(environment.urlProject + 'contratos/' + token);
  }

  putContrato(token, data) {
    console.log(data)
    return this.http.put<any>(environment.urlProject + 'contratos/' + token, data);
  }

  getMyContracts(token){
    console.log(token)
    return this.http.post<any>(environment.urlProject + '/api/miscontratos', token);
  }

  getHasContratoClausula(token){
    return this.http.get<any>(environment.urlProject + '/contratosclausulas/' + token);
    // return this.http.get<any>(environment.urlProject + '​/contratosclausulas​/' + token);
  }

  getContractosEmpresa(token) {
    console.log(token);
    return this.http.get<any>(environment.urlProject + '/api/contratosempresa/' + token)
    // return this.http.get<any>('http://127.0.0.1:​3011/api/contratosempresa/' + token);
  }

}
