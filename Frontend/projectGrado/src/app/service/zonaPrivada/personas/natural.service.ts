import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NaturalService {

  constructor(private http: HttpClient) { }

  postPersonaNatural(data) {
    return this.http.post<any>(environment.urlProject + 'personas', data);
  }

  postPersonaJuridica(data) {
    return this.http.post<any>(environment.urlProject + 'empresas', data);
  }

  postPersonaContacto(data) {
    return this.http.post<any>(environment.urlProject + 'contactos', data);
  }

  postPersonaFirmante(data) {
    return this.http.post<any>(environment.urlProject + 'firmacontratos', data);
  }

  postContratoParametros(data){
    return this.http.post<any>(environment.urlProject + 'contratospersonas', data);
  }

  postContratoClausulas(data){
    return this.http.post<any>(environment.urlProject + 'contratosclausulas', data);
  }

  putPersonaNatural(token, data) {
    return this.http.put<any>(environment.urlProject + 'personas/' + token, data);
  }

  putPersonaJuridica(token, data) {
    return this.http.put<any>(environment.urlProject + 'empresas/' + token, data);
  }

  putPersonaContacto(token, data) {
    return this.http.put<any>(environment.urlProject + 'contactos/' + token, data);
  }

  putPersonaFirmante(token, data) {
    return this.http.put<any>(environment.urlProject + 'firmacontratos/' + token, data);
  }

  getPersonaNatural(data) {
    return this.http.get<any>(environment.urlProject + 'personas/' + data);
  }

  getPersonaJuridica(data) {
    return this.http.get<any>(environment.urlProject + 'empresas/' + data);
  }

  getPersonaContacto(data) {
    return this.http.get<any>(environment.urlProject + 'contactos/' + data);
  }

  getPersonaFirmante(data) {
    return this.http.get<any>(environment.urlProject + 'firmacontratos/' + data);
  }

  getPersonaNaturalAll() {
    return this.http.get<any>(environment.urlProject + 'personas/');
  }

  getIdFirmante(idContratante) {
    return this.http.get<any>(environment.urlProject + 'api/firmacontratos/' + idContratante);
  }

}
