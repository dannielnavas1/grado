import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  constructor(private http: HttpClient) { }

  getPais() {
    return this.http.get<any>(environment.urlParametros + 'pais');
  }

  getDepartamento() {
    return this.http.get<any>(environment.urlParametros + 'departamentos');
  }

  getCiudad() {
    return this.http.get<any>(environment.urlParametros + 'ciudades');
  }

  getAlmacenContratos() {
    return this.http.get<any>(environment.urlParametros + '​/api​/almacencontratos');
  }

  getAlmacenContratosId(idContrato) {
    console.log(idContrato)
    return this.http.get<any>(environment.urlParametros + 'api/almacencontratosid/' + idContrato)
  }

  getCargos() {
    return this.http.get<any>(environment.urlParametros + 'api/cargos');
  }

  getClausulas() {
    console.log('Consulta')
    return this.http.get<any>( environment.urlParametros + 'api/clausulas');
  }

  getClausulasId(token) {
    console.log('Consulta')
    return this.http.get<any>( environment.urlParametros + 'api/clausulas/' + token);
  }

  getParametros() {
    return this.http.get<any>( environment.urlParametros + 'api/parametros');
  }

  getProfesiones() {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones');
  }

  getIdProfesiones(data) {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones/' + data);
  }

  getSectores() {
    return this.http.get<any>(environment.urlParametros + '/api/sectores');
  }

  getTipoContratos() {
    return this.http.get<any>(environment.urlParametros + '/api/tipocontratos');
  }

  getTipoDocumento() {
    return this.http.get<any>(environment.urlParametros + '/api/tipodocumentos');
  }

  getTipologia() {
    return this.http.get<any>(environment.urlParametros + '/api/tipologias');
  }


  // Counts parametos

  getPaisCount() {
    return this.http.get<any>(environment.urlParametros + 'pais/count');
  }

  getDepartamentoCount() {
    return this.http.get<any>(environment.urlParametros + 'departamentos/count');
  }

  getCiudadCount() {
    return this.http.get<any>(environment.urlParametros + 'ciudades/count');
  }

  getAlmacenContratosCount() {
    return this.http.get<any>(environment.urlParametros + '​/api​/almacencontratos/count');
  }

  getCargosCount() {
    return this.http.get<any>(environment.urlParametros + 'api/cargos/count');
  }

  getClausulasCount() {
    return this.http.get<any>(environment.urlParametros + '​/api/clausulas/count');
  }

  getParametrosCount() {
    return this.http.get<any>(environment.urlParametros + '​​/api​/parametros/count');
  }

  getProfesionesCount() {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones/count');
  }

  getSectoresCount() {
    return this.http.get<any>(environment.urlParametros + '/api/sectores/count');
  }

  getTipoContratosCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipocontratos/count');
  }

  getTipoDocumentoCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipodocumentos/count');
  }

  getTipologiaCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipologias/count');
  }

  // consultapor id

  getIdPais(id) {
    return this.http.get<any>( environment.urlParametros + 'pais/' + id);
  }

  getIdDepartamento(id) {
    return this.http.get<any>( environment.urlParametros + 'departamentos/' + id);
  }

  getIdCiudad(id) {
    return this.http.get<any>( environment.urlParametros + 'ciudades/' + id);
  }

  // Delete

  deleteTipoDocumento(id){
    return this.http.delete<any>(environment.urlParametros + '/api/tipodocumentos/' + id)
  }

  deleteTipologia(id){
    return this.http.delete<any>(environment.urlParametros + '/api/tipologias/' + id)
  }

  deleteTipoContrato(id){
    return this.http.delete<any>(environment.urlParametros + '/api/tipocontratos/' + id)
  }

  deleteClausualas(id) {
    return this.http.delete<any>(environment.urlParametros + '/api/clausulas/' + id)
  }

  // Post

  postTipoDocumento(data){
    return this.http.post<any>( environment.urlParametros + '/api/tipodocumentos/', data)
  }

  postTipologia(data){
    return this.http.post<any>( environment.urlParametros + '/api/tipologias/', data)
  }

  postTipoContrato(data) {
    return this.http.post<any>( environment.urlParametros + '/api/tipocontratos', data)
  }

  postAlmacenContratos(data) {
    return this.http.post<any>(environment.urlParametros + 'api/almacencontratos', data)
    // return this.http.post<any>(environment.urlParametros + '​/api​/almacencontratos', data)
  }

  postClausualas(data){
    return this.http.post<any>(environment.urlParametros + 'api/clausulas', data)
  }

}
