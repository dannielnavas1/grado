import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndexComponent } from './componets/publiczone/index/index.component';
import { LoginComponent } from './componets/publiczone/login/login.component';
import { RegisterComponent } from './componets/publiczone/register/register.component';
import { RecoveryComponent } from './componets/publiczone/recovery/recovery.component';
import { ChangeComponent } from './componets/publiczone/change/change.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    RegisterComponent,
    RecoveryComponent,
    ChangeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
