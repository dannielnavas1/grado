-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-11-2019 a las 21:00:54
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `grado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AlmacenContratos`
--

CREATE TABLE `AlmacenContratos` (
  `idAlmacenContratos` int(11) NOT NULL,
  `AlmacenContratos` longtext DEFAULT NULL,
  `TipoContrato_idTipoContrato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `AlmacenContratos`
--

INSERT INTO `AlmacenContratos` (`idAlmacenContratos`, `AlmacenContratos`, `TipoContrato_idTipoContrato`) VALUES
(1, 'Entre EL EMPLEADOR y EL TRABAJADOR, de las condiciones ya dichas, identificados. Como aparece al pie de sus firmas, se ha celebrado el presente contrato individual de trabajo, regido además por las siguientes cláusulas: PRIMERA: OBJETO. EL EMPLEADOR contrata los servicios personales del TRABAJADOR y éste se obliga: a) a poner al servicio del EMPLEADOR toda su capacidad normal de trabajo, en forma exclusiva en el desempeño de las funciones propias del oficio mencionado y en las labores anexas y complementadas del mismo, de conformidad con las órdenes e instrucciones que le imparta EL EMPLEADOR directamente o a través de sus representantes. b) a no prestar directa ni indirectamente servicios laborales a otros EMPLEADORES, ni a trabajar por cuenta propia en el mismo oficio, durante la vigencia de este contrato; y c) a guardar absoluta reserva sobre los hechos, documentos, informaciones y conocimiento por causa o con ocasión de su contrato de trabajo. PARAGRAFO: El TRABAJADOR desempeñará el cargo de {{ cargo }}, cumpliendo de manera general las siguientes actividades:\r\n\r\n•	Obrar con seriedad y diligencia en el servicio contratado \r\n•	Atender las solicitudes y recomendaciones que haga el CONTRATANTE o sus delegados, con la mayor prontitud posible\r\n•	Observar en el desarrollo del presente convenio los criterios técnicos y las recomendaciones dadas por el CONTRATANTE, de acuerdo a sus necesidades. \r\n•	Poner a disposición del CONTRATANTE, todos los conocimientos y recursos requeridos para el cabal cumplimiento del presente contrato en sus objetivos acordados entre las partes. \r\n•	Mantener informado al CONTRATANTE de toda comunicación que por razón de su prestación del servicio debe conocer. \r\n•	Cumplir oportunamente con las actividades a desarrollar contenidas en el presente convenio. \r\n•	Cumplir oportunamente con sus obligaciones de pago a la Seguridad Social. \r\n•	Presentar soportes de pago de Seguridad Social anexo con informe de actividades y productos desarrollados. \r\n•	Realizará entregas parciales con el fin de que EL CONTRATANTE evalué el avance de los productos a desarrollar y hacer correcciones necesarias, las fechas de entrega se definen de mutuo acuerdo.\r\n\r\nLa descripción anterior es general y no excluye ni limita para ejecutar labores conexas complementarias, accesorias o similares y en general aquellas que sean necesarias para un mejor resultado en la ejecución de la causa que dio origen al contrato, pudiendo en consecuencia complementar e implementar la descripción que por vía de ejemplo se establecen en este acuerdo. \r\nSEGUNDA: REMUNERACION. EL EMPLEADOR pagará al TRABAJADOR por la prestación de sus servicios el salario indicado, pagadero en las oportunidades mensualmente sin que ello signifique que unilateralmente el empleador pueda pagar por periodos menores. Dentro de la retribución acordada se encuentra incluida los descansos dominicales y festivos. PARAGRAFO: El trabajador autoriza al empleador para que la retribución así como cualquier otro beneficio, sea prestacional, descanso vacaciones etc. originado en la existencia y/o terminación del contrato sean consignadas o trasladadas a cuenta que desde ya el trabajador autoriza al empleador para que sea Abierta a su nombre en una institución financiera. TERCERA: TRABAJO NOCTURNO, SUPLEMENTARIO, DOMINICALY/O FESTIVO. Para el reconocimiento y pago del trabajo suplementario, nocturno, dominical o festivo, EL EMPLEADOR o sus representantes deberán haberlo autorizado previamente y por escrito. Cuando la necesidad de este trabajo se presente de manera imprevista o inaplazable, deberá ejecutarse y darse cuenta de él por escrito, a la mayor brevedad, al EMPLEADOR o a sus representantes para su aprobación. EL EMPLEADOR, en consecuencia, no reconocerá ningún trabajo suplementario, o trabajo nocturno o en días de descanso legalmente obligatorio que no haya sido autorizado previamente o que, habiendo sido avisado inmediatamente, no haya sido aprobado como queda dicho. El empleador fijara las jornadas laborales de acuerdo a las necesidades del servicio pudiendo variarlas durante la ejecución del presente contrato. CUARTA: JORNADA DE TRABAJO. EL TRABAJADOR se obliga a laborar la jornada máxima legal, salvo estipulación expresa y escrita en contrario, en los turnos y dentro de las horas señalados por el EMPLEADOR, pudiendo hacer éste ajustes o cambios de horario cuando lo estime conveniente. Por el acuerdo expreso o tácito de las partes, podrán repartirse las horas de la jornada ordinaria en la forma prevista en la ley, teniendo en cuenta que los tiempos de descanso entre las secciones de la jornada no se computan dentro de las mismas. QUINTA: PERIODO DE PRUEBA. Los primeros dos meses del presente contrato se consideran como período de prueba y, por consiguiente, cualquiera de las partes podrá terminar el contrato unilateralmente, en cualquier momento durante dicho período, sin que se cause el pago de indemnización alguna. SEXTA: DURACION DEL CONTRATO. La duración del contrato será indefinida, mientras subsistan las causas que le dieron origen y la materia del trabajo. SEPTIMA: TERMINACION UNILATERAL. Son justas causas para dar por terminado unilateralmente este contrato, por cualquiera de las partes, las que establece la Ley y el reglamento interno de Trabajo.OCTAVA: INVENCIONES. Las invenciones realizadas por EL TRABAJADOR le pertenecen a la empresa siempre y cuando estas sean realizadas con ocasión y dentro de la ejecución del contrato de trabajo, y como parte del cumplimiento de las obligaciones del cargo. También lo son aquellas que se obtienen mediante los datos y medios conocidos o utilizados en razón de la labor desempeñada. NOVENA: DERECHOS DE AUTOR. Los derechos patrimoniales de autor sobre las obras creadas por el TRABAJADOR en ejercicio de sus funciones o con ocasión ellas pertenecen al EMPLEADOR. Todo lo anterior sin perjuicio de los derechos morales de autor que permanecerán en cabeza del creador de la obra, de acuerdo con la ley 23 de 1982 y la Decisión 351 de la Comisión del Acuerdo de Cartagena. DÉCIMA: TRASLADOS: Desde ya el trabajador acuerda que el empleador podrá trasladarlo desde el lugar, cargo y/o sitio de trabajo de acuerdo a las necesidades del servicio siempre y cuando no se menos cabe el honor la dignidad o se produzca una desmejora sustancial o grave perjuicio con ocasión a la citada orden. El empleador esta obligado a asumir los gastos originados en el traslado. Siempre que sea una decisión unilateral de la empresa. DÉCIMA PRIMERA: BENEFICIOS EXTRALEGALES: El empleador podrá reconocer beneficios, primas, prestaciones de naturaleza extra legal, lo que se hace a titulo de mera liberalidad y estos subsistirán hasta que el empleador decida su modificación o supresión, atendiendo su capacidad, todos los cuales se otorgan y reconocen, y el trabajador así lo acuerdan sin que tengan carácter salarial y por lo tanto no tienen efecto prestacional o incidencia en la base de aportes en la seguridad social o parafiscal en especial este acuerdo se refiere a auxilios en dinero o en especie, primas periódicas o de antigüedad o en general beneficios de esa naturaleza los que podrán ser modificados o suprimidos por el empleador de acuerdo con su determinación unilateral tal como fue otorgado. DÉCIMA SEGUNDA: MODIFICACION DE LAS CONDICIONES LABORALES. El TRABAJADOR acepta desde ahora expresamente todas las modificaciones determinadas por el EMPLEADOR, en ejercicio de su poder subordinante, de sus condiciones laborales, tales como la jornada de trabajo, el lugar de prestación de servicio, el cargo u oficio y/o funciones y la forma de remuneración, siempre que tales modificaciones no afecten su honor, dignidad o sus derechos mínimos ni impliquen desmejoras sustanciales o graves perjuicios para él, de conformidad con lo dispuesto en la Ley. DÉCIMA TERCERA: EFECTOS. El presente contrato reemplaza en su integridad y deja sin efecto cualquiera otro contrato, verbal o escrito, celebrado entre las partes con anterioridad, pudiendo las partes convenir por escrito modificaciones al mismo, las que formarán parte integrante de este contrato.\r\n\r\nSe suscribe por las partes que intervienen en el acto, en señal de aceptación a {{ fechaContrato }}.\r\n', 2),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in dui egestas, ultrices eros eu, porta turpis. Vestibulum sed erat vitae justo rutrum dapibus eget non leo. Aenean laoreet urna dui, eget lobortis massa mattis eget. Quisque imperdiet, tellus elementum accumsan interdum, massa velit ornare lorem, et pharetra velit nisi id magna. Curabitur ut maximus turpis, vel ultricies urna. Pellentesque urna justo, varius nec rutrum sed, interdum quis eros. Phasellus lobortis convallis urna. Aenean nec convallis leo, ut dictum mi. Suspendisse justo orci, aliquet a eros vitae, gravida blandit ante. Maecenas sem nibh, mattis vel mattis et, posuere ut justo. Morbi bibendum nec purus nec ornare. Phasellus id odio libero. Nunc pretium nunc eget dolor consequat, a euismod est pharetra. Nunc feugiat nunc eget aliquet aliquam. Donec rutrum mi eu elit pretium, ut vulputate quam tincidunt.\n\nDuis molestie malesuada erat, ut vulputate mi imperdiet egestas. Sed finibus, lacus dignissim interdum dictum, ligula arcu faucibus sem, eget auctor nibh ipsum et libero. Donec dictum dapibus odio eu fringilla. Pellentesque placerat neque mi, sed imperdiet elit scelerisque in. Proin venenatis tempus risus a tristique. Donec arcu metus, fringilla sed sollicitudin vitae, rhoncus vitae nisi. Aenean leo neque, pharetra ac vulputate eu, ullamcorper ut libero. Morbi quam felis, aliquet vel gravida varius, elementum vitae risus.\n\nAliquam sagittis sed purus nec rhoncus. Aliquam augue orci, ullamcorper vitae lobortis vel, tincidunt non mauris. Cras non porta enim. Sed gravida nunc sed tristique fermentum. Sed et metus tincidunt, pulvinar nunc in, rhoncus nisl. Nam facilisis odio ac orci ultricies, nec eleifend sem ultrices. Suspendisse potenti. Nulla et scelerisque felis.\n\nVestibulum maximus felis vel erat convallis, vitae blandit nisl posuere. Aenean sagittis nunc a magna aliquet, et cursus velit viverra. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque mollis sapien eget ex suscipit porttitor. Nulla id quam tincidunt tellus pulvinar convallis eu efficitur tellus. Praesent volutpat augue vel ex rutrum ultricies. Phasellus bibendum dictum nisi at dignissim. Sed id sagittis velit. Vestibulum pharetra ullamcorper nulla tincidunt vestibulum.\n\nVestibulum quis sem rutrum, ultricies ex eget, condimentum orci. Curabitur malesuada est nibh, quis elementum nulla imperdiet cursus. Aliquam erat volutpat. Sed sagittis ullamcorper neque, ut imperdiet velit molestie sed. Aenean malesuada, nunc non egestas hendrerit, mauris lacus semper lacus, quis congue ipsum justo sit amet sem. Pellentesque vestibulum tincidunt augue. Nulla facilisi.', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cargo`
--

CREATE TABLE `Cargo` (
  `idCargo` int(11) NOT NULL,
  `Cargo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Cargo`
--

INSERT INTO `Cargo` (`idCargo`, `Cargo`) VALUES
(1, 'CEO'),
(2, 'Reclutador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Ciudad`
--

CREATE TABLE `Ciudad` (
  `idCiudad` int(11) NOT NULL,
  `Ciudad` varchar(45) DEFAULT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Ciudad`
--

INSERT INTO `Ciudad` (`idCiudad`, `Ciudad`, `Departamento_idDepartamento`) VALUES
(1, 'MEDELLIN', 1),
(2, 'ABEJORRAL', 1),
(3, 'ABRIAQUI', 1),
(4, 'ALEJANDRIA', 1),
(5, 'AMAGA', 1),
(6, 'AMALFI', 1),
(7, 'ANDES', 1),
(8, 'ANGELOPOLIS', 1),
(9, 'ANGOSTURA', 1),
(10, 'ANORI', 1),
(11, 'SANTAFE DE ANTIOQUIA', 1),
(12, 'ANZA', 1),
(13, 'APARTADO', 1),
(14, 'ARBOLETES', 1),
(15, 'ARGELIA', 1),
(16, 'ARMENIA', 1),
(17, 'BARBOSA', 1),
(18, 'BELMIRA', 1),
(19, 'BELLO', 1),
(20, 'BETANIA', 1),
(21, 'BETULIA', 1),
(22, 'CIUDAD BOLIVAR', 1),
(23, 'BRICEÑO', 1),
(24, 'BURITICA', 1),
(25, 'CACERES', 1),
(26, 'CAICEDO', 1),
(27, 'CALDAS', 1),
(28, 'CAMPAMENTO', 1),
(29, 'CAÑASGORDAS', 1),
(30, 'CARACOLI', 1),
(31, 'CARAMANTA', 1),
(32, 'CAREPA', 1),
(33, 'EL CARMEN DE VIBORAL', 1),
(34, 'CAROLINA', 1),
(35, 'CAUCASIA', 1),
(36, 'CHIGORODO', 1),
(37, 'CISNEROS', 1),
(38, 'COCORNA', 1),
(39, 'CONCEPCION', 1),
(40, 'CONCORDIA', 1),
(41, 'COPACABANA', 1),
(42, 'DABEIBA', 1),
(43, 'DON MATIAS', 1),
(44, 'EBEJICO', 1),
(45, 'EL BAGRE', 1),
(46, 'ENTRERRIOS', 1),
(47, 'ENVIGADO', 1),
(48, 'FREDONIA', 1),
(49, 'FRONTINO', 1),
(50, 'GIRALDO', 1),
(51, 'GIRARDOTA', 1),
(52, 'GOMEZ PLATA', 1),
(53, 'GRANADA', 1),
(54, 'GUADALUPE', 1),
(55, 'GUARNE', 1),
(56, 'GUATAPE', 1),
(57, 'HELICONIA', 1),
(58, 'HISPANIA', 1),
(59, 'ITAGUI', 1),
(60, 'ITUANGO', 1),
(61, 'JARDIN', 1),
(62, 'JERICO', 1),
(63, 'LA CEJA', 1),
(64, 'LA ESTRELLA', 1),
(65, 'LA PINTADA', 1),
(66, 'LA UNION', 1),
(67, 'LIBORINA', 1),
(68, 'MACEO', 1),
(69, 'MARINILLA', 1),
(70, 'MONTEBELLO', 1),
(71, 'MURINDO', 1),
(72, 'MUTATA', 1),
(73, 'NARIÑO', 1),
(74, 'NECOCLI', 1),
(75, 'NECHI', 1),
(76, 'OLAYA', 1),
(77, 'PEÐOL', 1),
(78, 'PEQUE', 1),
(79, 'PUEBLORRICO', 1),
(80, 'PUERTO BERRIO', 1),
(81, 'PUERTO NARE', 1),
(82, 'PUERTO TRIUNFO', 1),
(83, 'REMEDIOS', 1),
(84, 'RETIRO', 1),
(85, 'RIONEGRO', 1),
(86, 'SABANALARGA', 1),
(87, 'SABANETA', 1),
(88, 'SALGAR', 1),
(89, 'SAN ANDRES DE CUERQUIA', 1),
(90, 'SAN CARLOS', 1),
(91, 'SAN FRANCISCO', 1),
(92, 'SAN JERONIMO', 1),
(93, 'SAN JOSE DE LA MONTAÑA', 1),
(94, 'SAN JUAN DE URABA', 1),
(95, 'SAN LUIS', 1),
(96, 'SAN PEDRO', 1),
(97, 'SAN PEDRO DE URABA', 1),
(98, 'SAN RAFAEL', 1),
(99, 'SAN ROQUE', 1),
(100, 'SAN VICENTE', 1),
(101, 'SANTA BARBARA', 1),
(102, 'SANTA ROSA DE OSOS', 1),
(103, 'SANTO DOMINGO', 1),
(104, 'EL SANTUARIO', 1),
(105, 'SEGOVIA', 1),
(106, 'SONSON', 1),
(107, 'SOPETRAN', 1),
(108, 'TAMESIS', 1),
(109, 'TARAZA', 1),
(110, 'TARSO', 1),
(111, 'TITIRIBI', 1),
(112, 'TOLEDO', 1),
(113, 'TURBO', 1),
(114, 'URAMITA', 1),
(115, 'URRAO', 1),
(116, 'VALDIVIA', 1),
(117, 'VALPARAISO', 1),
(118, 'VEGACHI', 1),
(119, 'VENECIA', 1),
(120, 'VIGIA DEL FUERTE', 1),
(121, 'YALI', 1),
(122, 'YARUMAL', 1),
(123, 'YOLOMBO', 1),
(124, 'YONDO', 1),
(125, 'ZARAGOZA', 1),
(126, 'BARRANQUILLA', 3),
(127, 'BARANOA', 3),
(128, 'CAMPO DE LA CRUZ', 3),
(129, 'CANDELARIA', 3),
(130, 'GALAPA', 3),
(131, 'JUAN DE ACOSTA', 3),
(132, 'LURUACO', 3),
(133, 'MALAMBO', 3),
(134, 'MANATI', 3),
(135, 'PALMAR DE VARELA', 3),
(136, 'PIOJO', 3),
(137, 'POLONUEVO', 3),
(138, 'PONEDERA', 3),
(139, 'PUERTO COLOMBIA', 3),
(140, 'REPELON', 3),
(141, 'SABANAGRANDE', 3),
(142, 'SABANALARGA', 3),
(143, 'SANTA LUCIA', 3),
(144, 'SANTO TOMAS', 3),
(145, 'SOLEDAD', 3),
(146, 'SUAN', 3),
(147, 'TUBARA', 3),
(148, 'USIACURI', 3),
(149, 'BOGOTA, D.C.', 5),
(150, 'CARTAGENA', 7),
(151, 'ACHI', 7),
(152, 'ALTOS DEL ROSARIO', 7),
(153, 'ARENAL', 7),
(154, 'ARJONA', 7),
(155, 'ARROYOHONDO', 7),
(156, 'BARRANCO DE LOBA', 7),
(157, 'CALAMAR', 7),
(158, 'CANTAGALLO', 7),
(159, 'CICUCO', 7),
(160, 'CORDOBA', 7),
(161, 'CLEMENCIA', 7),
(162, 'EL CARMEN DE BOLIVAR', 7),
(163, 'EL GUAMO', 7),
(164, 'EL PEÑON', 7),
(165, 'HATILLO DE LOBA', 7),
(166, 'MAGANGUE', 7),
(167, 'MAHATES', 7),
(168, 'MARGARITA', 7),
(169, 'MARIA LA BAJA', 7),
(170, 'MONTECRISTO', 7),
(171, 'MOMPOS', 7),
(172, 'NOROSI', 7),
(173, 'MORALES', 7),
(174, 'PINILLOS', 7),
(175, 'REGIDOR', 7),
(176, 'RIO VIEJO', 7),
(177, 'SAN CRISTOBAL', 7),
(178, 'SAN ESTANISLAO', 7),
(179, 'SAN FERNANDO', 7),
(180, 'SAN JACINTO', 7),
(181, 'SAN JACINTO DEL CAUCA', 7),
(182, 'SAN JUAN NEPOMUCENO', 7),
(183, 'SAN MARTIN DE LOBA', 7),
(184, 'SAN PABLO', 7),
(185, 'SANTA CATALINA', 7),
(186, 'SANTA ROSA', 7),
(187, 'SANTA ROSA DEL SUR', 7),
(188, 'SIMITI', 7),
(189, 'SOPLAVIENTO', 7),
(190, 'TALAIGUA NUEVO', 7),
(191, 'TIQUISIO', 7),
(192, 'TURBACO', 7),
(193, 'TURBANA', 7),
(194, 'VILLANUEVA', 7),
(195, 'ZAMBRANO', 7),
(196, 'TUNJA', 9),
(197, 'ALMEIDA', 9),
(198, 'AQUITANIA', 9),
(199, 'ARCABUCO', 9),
(200, 'BELEN', 9),
(201, 'BERBEO', 9),
(202, 'BETEITIVA', 9),
(203, 'BOAVITA', 9),
(204, 'BOYACA', 9),
(205, 'BRICEÑO', 9),
(206, 'BUENAVISTA', 9),
(207, 'BUSBANZA', 9),
(208, 'CALDAS', 9),
(209, 'CAMPOHERMOSO', 9),
(210, 'CERINZA', 9),
(211, 'CHINAVITA', 9),
(212, 'CHIQUINQUIRA', 9),
(213, 'CHISCAS', 9),
(214, 'CHITA', 9),
(215, 'CHITARAQUE', 9),
(216, 'CHIVATA', 9),
(217, 'CIENEGA', 9),
(218, 'COMBITA', 9),
(219, 'COPER', 9),
(220, 'CORRALES', 9),
(221, 'COVARACHIA', 9),
(222, 'CUBARA', 9),
(223, 'CUCAITA', 9),
(224, 'CUITIVA', 9),
(225, 'CHIQUIZA', 9),
(226, 'CHIVOR', 9),
(227, 'DUITAMA', 9),
(228, 'EL COCUY', 9),
(229, 'EL ESPINO', 9),
(230, 'FIRAVITOBA', 9),
(231, 'FLORESTA', 9),
(232, 'GACHANTIVA', 9),
(233, 'GAMEZA', 9),
(234, 'GARAGOA', 9),
(235, 'GUACAMAYAS', 9),
(236, 'GUATEQUE', 9),
(237, 'GUAYATA', 9),
(238, 'GsICAN', 9),
(239, 'IZA', 9),
(240, 'JENESANO', 9),
(241, 'JERICO', 9),
(242, 'LABRANZAGRANDE', 9),
(243, 'LA CAPILLA', 9),
(244, 'LA VICTORIA', 9),
(245, 'LA UVITA', 9),
(246, 'VILLA DE LEYVA', 9),
(247, 'MACANAL', 9),
(248, 'MARIPI', 9),
(249, 'MIRAFLORES', 9),
(250, 'MONGUA', 9),
(251, 'MONGUI', 9),
(252, 'MONIQUIRA', 9),
(253, 'MOTAVITA', 9),
(254, 'MUZO', 9),
(255, 'NOBSA', 9),
(256, 'NUEVO COLON', 9),
(257, 'OICATA', 9),
(258, 'OTANCHE', 9),
(259, 'PACHAVITA', 9),
(260, 'PAEZ', 9),
(261, 'PAIPA', 9),
(262, 'PAJARITO', 9),
(263, 'PANQUEBA', 9),
(264, 'PAUNA', 9),
(265, 'PAYA', 9),
(266, 'PAZ DE RIO', 9),
(267, 'PESCA', 9),
(268, 'PISBA', 9),
(269, 'PUERTO BOYACA', 9),
(270, 'QUIPAMA', 9),
(271, 'RAMIRIQUI', 9),
(272, 'RAQUIRA', 9),
(273, 'RONDON', 9),
(274, 'SABOYA', 9),
(275, 'SACHICA', 9),
(276, 'SAMACA', 9),
(277, 'SAN EDUARDO', 9),
(278, 'SAN JOSE DE PARE', 9),
(279, 'SAN LUIS DE GACENO', 9),
(280, 'SAN MATEO', 9),
(281, 'SAN MIGUEL DE SEMA', 9),
(282, 'SAN PABLO DE BORBUR', 9),
(283, 'SANTANA', 9),
(284, 'SANTA MARIA', 9),
(285, 'SANTA ROSA DE VITERBO', 9),
(286, 'SANTA SOFIA', 9),
(287, 'SATIVANORTE', 9),
(288, 'SATIVASUR', 9),
(289, 'SIACHOQUE', 9),
(290, 'SOATA', 9),
(291, 'SOCOTA', 9),
(292, 'SOCHA', 9),
(293, 'SOGAMOSO', 9),
(294, 'SOMONDOCO', 9),
(295, 'SORA', 9),
(296, 'SOTAQUIRA', 9),
(297, 'SORACA', 9),
(298, 'SUSACON', 9),
(299, 'SUTAMARCHAN', 9),
(300, 'SUTATENZA', 9),
(301, 'TASCO', 9),
(302, 'TENZA', 9),
(303, 'TIBANA', 9),
(304, 'TIBASOSA', 9),
(305, 'TINJACA', 9),
(306, 'TIPACOQUE', 9),
(307, 'TOCA', 9),
(308, 'TOGsI', 9),
(309, 'TOPAGA', 9),
(310, 'TOTA', 9),
(311, 'TUNUNGUA', 9),
(312, 'TURMEQUE', 9),
(313, 'TUTA', 9),
(314, 'TUTAZA', 9),
(315, 'UMBITA', 9),
(316, 'VENTAQUEMADA', 9),
(317, 'VIRACACHA', 9),
(318, 'ZETAQUIRA', 9),
(319, 'MANIZALES', 11),
(320, 'AGUADAS', 11),
(321, 'ANSERMA', 11),
(322, 'ARANZAZU', 11),
(323, 'BELALCAZAR', 11),
(324, 'CHINCHINA', 11),
(325, 'FILADELFIA', 11),
(326, 'LA DORADA', 11),
(327, 'LA MERCED', 11),
(328, 'MANZANARES', 11),
(329, 'MARMATO', 11),
(330, 'MARQUETALIA', 11),
(331, 'MARULANDA', 11),
(332, 'NEIRA', 11),
(333, 'NORCASIA', 11),
(334, 'PACORA', 11),
(335, 'PALESTINA', 11),
(336, 'PENSILVANIA', 11),
(337, 'RIOSUCIO', 11),
(338, 'RISARALDA', 11),
(339, 'SALAMINA', 11),
(340, 'SAMANA', 11),
(341, 'SAN JOSE', 11),
(342, 'SUPIA', 11),
(343, 'VICTORIA', 11),
(344, 'VILLAMARIA', 11),
(345, 'VITERBO', 11),
(346, 'FLORENCIA', 13),
(347, 'ALBANIA', 13),
(348, 'BELEN DE LOS ANDAQUIES', 13),
(349, 'CARTAGENA DEL CHAIRA', 13),
(350, 'CURILLO', 13),
(351, 'EL DONCELLO', 13),
(352, 'EL PAUJIL', 13),
(353, 'LA MONTAÑITA', 13),
(354, 'MILAN', 13),
(355, 'MORELIA', 13),
(356, 'PUERTO RICO', 13),
(357, 'SAN JOSE DEL FRAGUA', 13),
(358, 'SAN VICENTE DEL CAGUAN', 13),
(359, 'SOLANO', 13),
(360, 'SOLITA', 13),
(361, 'VALPARAISO', 13),
(362, 'POPAYAN', 15),
(363, 'ALMAGUER', 15),
(364, 'ARGELIA', 15),
(365, 'BALBOA', 15),
(366, 'BOLIVAR', 15),
(367, 'BUENOS AIRES', 15),
(368, 'CAJIBIO', 15),
(369, 'CALDONO', 15),
(370, 'CALOTO', 15),
(371, 'CORINTO', 15),
(372, 'EL TAMBO', 15),
(373, 'FLORENCIA', 15),
(374, 'GUACHENE', 15),
(375, 'GUAPI', 15),
(376, 'INZA', 15),
(377, 'JAMBALO', 15),
(378, 'LA SIERRA', 15),
(379, 'LA VEGA', 15),
(380, 'LOPEZ', 15),
(381, 'MERCADERES', 15),
(382, 'MIRANDA', 15),
(383, 'MORALES', 15),
(384, 'PADILLA', 15),
(385, 'PAEZ', 15),
(386, 'PATIA', 15),
(387, 'PIAMONTE', 15),
(388, 'PIENDAMO', 15),
(389, 'PUERTO TEJADA', 15),
(390, 'PURACE', 15),
(391, 'ROSAS', 15),
(392, 'SAN SEBASTIAN', 15),
(393, 'SANTANDER DE QUILICHAO', 15),
(394, 'SANTA ROSA', 15),
(395, 'SILVIA', 15),
(396, 'SOTARA', 15),
(397, 'SUAREZ', 15),
(398, 'SUCRE', 15),
(399, 'TIMBIO', 15),
(400, 'TIMBIQUI', 15),
(401, 'TORIBIO', 15),
(402, 'TOTORO', 15),
(403, 'VILLA RICA', 15),
(404, 'VALLEDUPAR', 17),
(405, 'AGUACHICA', 17),
(406, 'AGUSTIN CODAZZI', 17),
(407, 'ASTREA', 17),
(408, 'BECERRIL', 17),
(409, 'BOSCONIA', 17),
(410, 'CHIMICHAGUA', 17),
(411, 'CHIRIGUANA', 17),
(412, 'CURUMANI', 17),
(413, 'EL COPEY', 17),
(414, 'EL PASO', 17),
(415, 'GAMARRA', 17),
(416, 'GONZALEZ', 17),
(417, 'LA GLORIA', 17),
(418, 'LA JAGUA DE IBIRICO', 17),
(419, 'MANAURE', 17),
(420, 'PAILITAS', 17),
(421, 'PELAYA', 17),
(422, 'PUEBLO BELLO', 17),
(423, 'RIO DE ORO', 17),
(424, 'LA PAZ', 17),
(425, 'SAN ALBERTO', 17),
(426, 'SAN DIEGO', 17),
(427, 'SAN MARTIN', 17),
(428, 'TAMALAMEQUE', 17),
(429, 'MONTERIA', 19),
(430, 'AYAPEL', 19),
(431, 'BUENAVISTA', 19),
(432, 'CANALETE', 19),
(433, 'CERETE', 19),
(434, 'CHIMA', 19),
(435, 'CHINU', 19),
(436, 'CIENAGA DE ORO', 19),
(437, 'COTORRA', 19),
(438, 'LA APARTADA', 19),
(439, 'LORICA', 19),
(440, 'LOS CORDOBAS', 19),
(441, 'MOMIL', 19),
(442, 'MONTELIBANO', 19),
(443, 'MOÑITOS', 19),
(444, 'PLANETA RICA', 19),
(445, 'PUEBLO NUEVO', 19),
(446, 'PUERTO ESCONDIDO', 19),
(447, 'PUERTO LIBERTADOR', 19),
(448, 'PURISIMA', 19),
(449, 'SAHAGUN', 19),
(450, 'SAN ANDRES SOTAVENTO', 19),
(451, 'SAN ANTERO', 19),
(452, 'SAN BERNARDO DEL VIENTO', 19),
(453, 'SAN CARLOS', 19),
(454, 'SAN PELAYO', 19),
(455, 'TIERRALTA', 19),
(456, 'VALENCIA', 19),
(457, 'AGUA DE DIOS', 21),
(458, 'ALBAN', 21),
(459, 'ANAPOIMA', 21),
(460, 'ANOLAIMA', 21),
(461, 'ARBELAEZ', 21),
(462, 'BELTRAN', 21),
(463, 'BITUIMA', 21),
(464, 'BOJACA', 21),
(465, 'CABRERA', 21),
(466, 'CACHIPAY', 21),
(467, 'CAJICA', 21),
(468, 'CAPARRAPI', 21),
(469, 'CAQUEZA', 21),
(470, 'CARMEN DE CARUPA', 21),
(471, 'CHAGUANI', 21),
(472, 'CHIA', 21),
(473, 'CHIPAQUE', 21),
(474, 'CHOACHI', 21),
(475, 'CHOCONTA', 21),
(476, 'COGUA', 21),
(477, 'COTA', 21),
(478, 'CUCUNUBA', 21),
(479, 'EL COLEGIO', 21),
(480, 'EL PEÑON', 21),
(481, 'EL ROSAL', 21),
(482, 'FACATATIVA', 21),
(483, 'FOMEQUE', 21),
(484, 'FOSCA', 21),
(485, 'FUNZA', 21),
(486, 'FUQUENE', 21),
(487, 'FUSAGASUGA', 21),
(488, 'GACHALA', 21),
(489, 'GACHANCIPA', 21),
(490, 'GACHETA', 21),
(491, 'GAMA', 21),
(492, 'GIRARDOT', 21),
(493, 'GRANADA', 21),
(494, 'GUACHETA', 21),
(495, 'GUADUAS', 21),
(496, 'GUASCA', 21),
(497, 'GUATAQUI', 21),
(498, 'GUATAVITA', 21),
(499, 'GUAYABAL DE SIQUIMA', 21),
(500, 'GUAYABETAL', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Clausulas`
--

CREATE TABLE `Clausulas` (
  `idClausulas` int(11) NOT NULL,
  `TipoClausula` longtext NOT NULL,
  `Clausulas` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Clausulas`
--

INSERT INTO `Clausulas` (`idClausulas`, `TipoClausula`, `Clausulas`) VALUES
(1, 'Confidencialidad', 'CLAÚSULAS\r\nPRIMERA. El CONTRATISTA se obliga a no divulgar a terceras partes, la “Información confidencial”, que\r\nreciba por parte de COLCIENCIAS, y a darle a dicha información el mismo tratamiento que le darían a la\r\ninformación confidencial de su propiedad. Para efectos de la presente acta, “Información Confidencial”\r\ncomprende toda la información divulgada por COLCIENCIAS ya sea en forma oral, visual, escrita, grabada en\r\nmedios magnéticos o en cualquier otra forma tangible y que se encuentre claramente marcada como tal al ser\r\nentregada a la parte receptora.\r\nSEGUNDA. La parte receptora se obliga a mantener de manera confidencial la “Información confidencial” que\r\nreciba de COLCIENCIAS y a no darla a una tercera parte diferente de su equipo de trabajo y asesores que\r\ntengan la necesidad de conocer dicha información para los propósitos autorizados, y quienes deberán estar\r\nde acuerdo en mantener de manera confidencial dicha información.\r\nTERCERA. Es obligación del CONTRATISTA de no divulgar la “Información confidencial”, incluyendo, mas no\r\nlimitando, el informar a sus empleados que la manejen, que dicha información es confidencial y que no deberá\r\nser divulgada a terceras partes.\r\nCUARTA. El CONTRATISTA se obliga a utilizar la “Información confidencial” recibida, únicamente para el\r\ndesarrollo el objeto del contrato de fiducia mercantil suscrito con COLCIENCIAS.\r\nQUINTA. El CONTRATISTA se compromete a efectuar una adecuada custodia y reserva de la información y\r\ngestión -es decir tratamiento- de los datos suministrados por COLCIENCIAS al interior de las redes y bases\r\nde datos (físicas y/o electrónicas) en donde se realice su recepción y tratamiento en general.\r\nSEXTA. Para el caso del manejo de información que incluya datos personales, el CONTRATISTA dará\r\nestricto cumplimiento a las disposiciones constitucionales y legales sobre la protección del derecho\r\nfundamental de habeas data, en particular lo dispuesto en el artículo 15 de la Constitución Política y la ley\r\n1581 de 2012.\r\nSÉPTIMA. En caso de que el CONTRATISTA incumpla parcial o totalmente con las obligaciones establecidas\r\nen la presente acta éste será responsable de los daños y perjuicios que dicho incumplimiento llegase a\r\nocasionar a COLCIENCIAS.\r\nOCTAVA. La vigencia de la presente acta será indefinida y permanecerá vigente mientras exista relación\r\nreceptora, se hará acreedora a la Pena Convencional establecida en la Cláusula Séptima del presente\r\nContrato. '),
(2, 'Clausula de manejo de personal', NULL),
(3, 'Clausula de cumplimiento', 'ejemplo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contacto`
--

CREATE TABLE `Contacto` (
  `IdContacto` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Firmado` tinyint(4) NOT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL,
  `Cargo_idCargo` int(11) NOT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Contacto`
--

INSERT INTO `Contacto` (`IdContacto`, `Nombre`, `Apellido`, `Email`, `Firmado`, `TipoDocumento_idTipoDocumento`, `Cargo_idCargo`, `Empresa_nitEmpresa`) VALUES
(1022376965, 'Leslye Juliet', 'Navarro Ortega', 'leslye27navarro@gmail.com', 0, 1, 1, 1019042185);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contratos`
--

CREATE TABLE `Contratos` (
  `idContratos` bigint(20) NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaFinal` date NOT NULL,
  `FechaContrato` date DEFAULT NULL,
  `Salario` bigint(20) NOT NULL,
  `firmaContratoContratado` tinyint(1) DEFAULT NULL,
  `TipoContrato_idTipoContrato` int(11) NOT NULL,
  `FirmaContrato_Documento` bigint(20) DEFAULT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL,
  `Persona_idPersona` bigint(20) NOT NULL,
  `EstadoContrato_idEstadoContrato` int(11) NOT NULL,
  `Profesion_idProfesion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Contratos`
--

INSERT INTO `Contratos` (`idContratos`, `FechaInicio`, `FechaFinal`, `FechaContrato`, `Salario`, `firmaContratoContratado`, `TipoContrato_idTipoContrato`, `FirmaContrato_Documento`, `Empresa_nitEmpresa`, `Persona_idPersona`, `EstadoContrato_idEstadoContrato`, `Profesion_idProfesion`) VALUES
(4, '2019-10-24', '2019-12-31', '2019-10-24', 0, 1, 2, 1022376965, 1019042185, 1019042186, 2, 0),
(5, '2019-10-01', '2019-12-20', '2019-10-01', 0, 0, 2, 1022376965, 1019042185, 1019042186, 1, 0),
(6, '2019-10-24', '2019-10-25', '2019-10-24', 0, 0, 3, 1022376965, 1019042185, 1019042186, 1, 0),
(7, '2019-11-06', '2021-11-06', '2019-11-06', 1000000, 1, 2, 1022376965, 1019042185, 1019042186, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ContratosHasClausulas`
--

CREATE TABLE `ContratosHasClausulas` (
  `Contratos_idContratos` bigint(20) NOT NULL,
  `Clausulas_idClausulas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ContratosHasClausulas`
--

INSERT INTO `ContratosHasClausulas` (`Contratos_idContratos`, `Clausulas_idClausulas`) VALUES
(4, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ContratosHasParametros`
--

CREATE TABLE `ContratosHasParametros` (
  `Contratos_idContratos` bigint(20) NOT NULL,
  `Parametros_idParametros` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ContratosHasParametros`
--

INSERT INTO `ContratosHasParametros` (`Contratos_idContratos`, `Parametros_idParametros`) VALUES
(6, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Departamento`
--

CREATE TABLE `Departamento` (
  `idDepartamento` int(11) NOT NULL,
  `Departamento` varchar(45) DEFAULT NULL,
  `Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Departamento`
--

INSERT INTO `Departamento` (`idDepartamento`, `Departamento`, `Pais_idPais`) VALUES
(1, 'ANTIOQUIA', 169),
(3, 'ATLANTICO', 169),
(5, 'BOGOTA', 169),
(7, 'BOLIVAR', 169),
(9, 'BOYACA', 169),
(11, 'CALDAS', 169),
(13, 'CAQUETA', 169),
(15, 'CAUCA', 169),
(17, 'CESAR', 169),
(19, 'CORDOBA', 169),
(21, 'CUNDINAMARCA', 169),
(23, 'CHOCO', 169),
(25, 'HUILA', 169),
(27, 'LA GUAJIRA', 169),
(29, 'MAGDALENA', 169),
(31, 'META', 169),
(33, 'NARIÑO', 169),
(35, 'N . DE SANTANDER', 169),
(37, 'QUINDIO', 169),
(39, 'RISARALDA', 169),
(41, 'SANTANDER', 169),
(43, 'SUCRE', 169),
(45, 'TOLIMA', 169),
(47, 'VALLE DEL CAUCA', 169),
(49, 'ARAUCA', 169);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Empresa`
--

CREATE TABLE `Empresa` (
  `nitEmpresa` bigint(20) NOT NULL,
  `Nombre` longtext DEFAULT NULL,
  `numeroEmpleados` varchar(45) DEFAULT NULL,
  `Direccion` longtext DEFAULT NULL,
  `Pagina` varchar(45) DEFAULT NULL,
  `Sector_idSector` int(11) NOT NULL,
  `Tipologia_idTipologia` int(11) NOT NULL,
  `Pais_idPais` int(11) NOT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL,
  `Ciudad_idCiudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Empresa`
--

INSERT INTO `Empresa` (`nitEmpresa`, `Nombre`, `numeroEmpleados`, `Direccion`, `Pagina`, `Sector_idSector`, `Tipologia_idTipologia`, `Pais_idPais`, `Departamento_idDepartamento`, `Ciudad_idCiudad`) VALUES
(1019042185, 'DanielSAS', '19', 'calle 2 #93D - 30', 'danniel.com', 3, 2, 169, 5, 149);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EstadoContrato`
--

CREATE TABLE `EstadoContrato` (
  `idEstadoContrato` int(11) NOT NULL,
  `EstadoContratocol` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `EstadoContrato`
--

INSERT INTO `EstadoContrato` (`idEstadoContrato`, `EstadoContratocol`) VALUES
(1, 'Pendiente'),
(2, 'Finalizado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FirmaContrato`
--

CREATE TABLE `FirmaContrato` (
  `Documento` bigint(20) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellidos` varchar(45) DEFAULT NULL,
  `FirmaContrato` varchar(45) DEFAULT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `FirmaContrato`
--

INSERT INTO `FirmaContrato` (`Documento`, `Nombre`, `Apellidos`, `FirmaContrato`, `TipoDocumento_idTipoDocumento`, `Empresa_nitEmpresa`) VALUES
(1022376965, 'Leslye Juliet', 'Navarro Ortega', 'Leslye Navarro', 1, 1019042185);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pais`
--

CREATE TABLE `Pais` (
  `idPais` int(11) NOT NULL,
  `Pais` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Pais`
--

INSERT INTO `Pais` (`idPais`, `Pais`) VALUES
(13, 'AFGANISTAN'),
(17, 'ALBANIA'),
(23, 'ALEMANIA'),
(26, 'ARMENIA'),
(27, 'ARUBA'),
(29, 'BOSNIA Y HERZEGOVINA'),
(31, 'BURKINA FASO'),
(37, 'ANDORRA'),
(40, 'ANGOLA'),
(41, 'ANGUILA'),
(43, 'ANTIGUA Y BARBUDA'),
(53, 'ARABIA SAUDITA'),
(59, 'ARGELIA'),
(63, 'ARGENTINA'),
(69, 'AUSTRALIA'),
(72, 'AUSTRIA'),
(74, 'AZERBAIYAN'),
(77, 'BAHAMAS'),
(80, 'BAREIN'),
(81, 'BANGLADES'),
(83, 'BARBADOS'),
(87, 'BELGICA'),
(88, 'BELICE'),
(90, 'BERMUDAS'),
(91, 'BIELORRUSIA'),
(93, 'BIRMANIA'),
(97, 'ESTADO PLURINACIONAL DE BOLIVIA'),
(101, 'BOTSUANA'),
(105, 'BRASIL'),
(108, 'BRUNEI'),
(111, 'BULGARIA'),
(115, 'BURUNDI'),
(119, 'BUTAN'),
(127, 'CABO VERDE'),
(137, 'ISLAS CAIMAN'),
(141, 'CAMBOYA'),
(145, 'CAMERUN'),
(149, 'CANADA'),
(159, 'CIUDAD DEL VATICANO'),
(165, 'ISLAS COCOS'),
(169, 'COLOMBIA'),
(173, 'COMORAS'),
(177, 'REPUBLICA DEL CONGO'),
(183, 'ISLAS COOK'),
(187, 'REPUBLICA DEMOCRATICA '),
(190, 'REPUBLICA DE'),
(193, 'COSTA DE MARFIL'),
(196, 'COSTA RICA'),
(198, 'CROACIA'),
(199, 'CUBA'),
(203, 'CHAD'),
(211, 'CHILE'),
(215, 'CHINA'),
(218, 'PROVINCIA DE CHINA'),
(221, 'CHIPRE'),
(229, 'BENIN'),
(232, 'DINAMARCA'),
(235, 'DOMINICA'),
(239, 'ECUADOR'),
(240, 'EGIPTO'),
(242, 'EL SALVADOR'),
(243, 'ERITREA'),
(244, 'EMIRATOS ARABES UNIDOS'),
(245, 'ESPA�A'),
(246, 'ESLOVAQUIA'),
(247, 'ESLOVENIA'),
(249, 'ESTADOS UNIDOS'),
(251, 'ESTONIA'),
(253, 'ETIOPIA'),
(259, 'ISLAS FEROE'),
(267, 'FILIPINAS'),
(271, 'FINLANDIA'),
(275, 'FRANCIA'),
(281, 'GABON'),
(285, 'GAMBIA'),
(287, 'GEORGIA'),
(289, 'GHANA'),
(293, 'GIBRALTAR'),
(297, 'GRANADA'),
(301, 'GRECIA'),
(305, 'GROENLANDIA'),
(309, 'GUADALUPE'),
(313, 'GUAM'),
(317, 'GUATEMALA'),
(325, 'GUAYANA FRANCESA'),
(329, 'GUINEA'),
(331, 'GUINEA ECUATORIAL'),
(334, 'GUINEA-BISAU'),
(337, 'GUYANA'),
(341, 'HAITI'),
(345, 'HONDURAS'),
(351, 'HONG KONG'),
(355, 'HUNGRIA'),
(361, 'INDIA'),
(365, 'INDONESIA'),
(369, 'IRAK'),
(372, 'REPUBLICA ISLAMICA DE'),
(375, 'IRLANDA'),
(379, 'ISLANDIA'),
(383, 'ISRAEL'),
(386, 'ITALIA'),
(391, 'JAMAICA'),
(399, 'JAPON'),
(403, 'JORDANIA'),
(406, 'KAZAJISTAN'),
(410, 'KENIA'),
(411, 'KIRIBATI'),
(412, 'KIRGUISTAN'),
(413, 'KUWAIT'),
(420, 'REPUBLICA DEMOCRATICA POPULAR'),
(426, 'LESOTO'),
(429, 'LETONIA'),
(431, 'LIBANO'),
(434, 'LIBERIA'),
(438, 'LIBIA'),
(440, 'LIECHTENSTEIN'),
(443, 'LITUANIA'),
(445, 'LUXEMBURGO'),
(447, 'MACAO'),
(448, 'LA ANTIGUA REPUBLICA YUGOSLAVADE'),
(450, 'MADAGASCAR'),
(455, 'MALASIA'),
(458, 'MALAUI'),
(461, 'MALDIVAS'),
(464, 'MALI'),
(467, 'MALTA'),
(469, 'ISLAS MARIANAS DEL NORTE'),
(472, 'ISLAS MARSHALL'),
(474, 'MARRUECOS'),
(477, 'MARTINICA'),
(485, 'MAURICIO'),
(488, 'MAURITANIA'),
(493, 'MEXICO'),
(494, 'ESTADOS FEDERADOS DE'),
(496, 'REPUBLICA DE'),
(497, 'MONGOLIA'),
(498, 'MONACO'),
(501, 'MONTSERRAT'),
(505, 'MOZAMBIQUE'),
(507, 'NAMIBIA'),
(508, 'NAURU'),
(511, 'ISLA DE NAVIDAD'),
(517, 'NEPAL'),
(521, 'NICARAGUA'),
(525, 'NIGER'),
(528, 'NIGERIA'),
(531, 'NIUE'),
(535, 'NORFOLK'),
(538, 'NORUEGA'),
(542, 'NUEVA CALEDONIA'),
(545, 'PAPUA NUEVA GUINEA'),
(548, 'NUEVA ZELANDA'),
(551, 'VANUATU'),
(556, 'OMAN'),
(566, 'ISLAS ULTRAMARINAS DE ESTADOS UNIDOS'),
(573, 'PAISES BAJOS'),
(576, 'PAKISTAN'),
(578, 'PALAOS'),
(579, 'ESTADO DE'),
(580, 'PANAMA'),
(586, 'PARAGUAY'),
(589, 'PERU'),
(593, 'ISLAS PITCAIRN'),
(599, 'POLINESIA FRANCESA'),
(603, 'POLONIA'),
(607, 'PORTUGAL'),
(611, 'PUERTO RICO'),
(618, 'CATAR'),
(628, 'REINO UNIDO'),
(640, 'REPUBLICA CENTROAFRICANA'),
(644, 'REPUBLICA CHECA'),
(647, 'REPUBLICA DOMINICANA'),
(660, 'REUNION'),
(665, 'ZIMBABUE'),
(670, 'RUMANIA'),
(675, 'RUANDA'),
(676, 'FEDERACION DE'),
(677, 'ISLAS SALOMON'),
(685, 'SUDAN'),
(687, 'SAMOA'),
(690, 'SAMOA AMERICANA'),
(695, 'SAN CRISTOBAL Y NIEVES'),
(697, 'SAN MARINO'),
(700, 'SAN PEDRO Y MIQUELON'),
(705, 'SAN VICENTE Y LAS GRANADINAS'),
(710, 'ASCENSION Y TRISTAN DE ACU�A'),
(715, 'SANTA LUCIA'),
(720, 'SANTO TOME Y PRINCIPE'),
(728, 'SENEGAL'),
(731, 'SEYCHELLES'),
(735, 'SIERRA LEONA'),
(741, 'SINGAPUR'),
(744, 'REPUBLICA ARABE'),
(748, 'SOMALIA'),
(750, 'SRI LANKA'),
(756, 'SUDAFRICA'),
(759, 'REPUBLICA ARABE SAHARAUI DEMOCRATICA'),
(764, 'SUECIA'),
(767, 'SUIZA'),
(770, 'SURINAM'),
(773, 'SUAZILANDIA'),
(774, 'TAYIKISTAN'),
(776, 'TAILANDIA'),
(780, 'REPUBLICA UNIDA DE'),
(783, 'YIBUTI'),
(787, 'TERRITORIO BRITANICO DEL OCEANO INDICO'),
(788, 'TIMOR ORIENTAL'),
(800, 'TOGO'),
(805, 'TOKELAU'),
(810, 'TONGA'),
(815, 'TRINIDAD Y TOBAGO'),
(820, 'TUNEZ'),
(823, 'ISLAS TURCAS Y CAICOS'),
(825, 'TURKMENISTAN'),
(827, 'TURQUIA'),
(828, 'TUVALU'),
(830, 'UCRANIA'),
(833, 'UGANDA'),
(845, 'URUGUAY'),
(847, 'UZBEKISTAN'),
(850, 'REPUBLICA BOLIVARIANA DE'),
(855, 'VIETNAM'),
(863, 'ISLAS VIRGENES BRITANICAS'),
(866, 'ISLAS VIRGENES DE LOS ESTADOS UNIDOS'),
(870, 'FIYI'),
(875, 'WALLIS Y FUTUNA'),
(880, 'YEMEN'),
(885, 'YUGOSLAVIA'),
(888, 'REPUBLICA DEMOCRATICA DEL'),
(890, 'ZAMBIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Parametros`
--

CREATE TABLE `Parametros` (
  `idParametros` int(11) NOT NULL,
  `Parametros` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Parametros`
--

INSERT INTO `Parametros` (`idParametros`, `Parametros`) VALUES
(1, 'Salario'),
(2, 'Fecha de entrega');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Persona`
--

CREATE TABLE `Persona` (
  `idPersona` bigint(20) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Direccion` longtext DEFAULT NULL,
  `FirmaContratoPersona` longtext DEFAULT NULL,
  `Profesion_idProfesion` int(11) NOT NULL,
  `Pais_idPais` int(11) NOT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL,
  `Ciudad_idCiudad` int(11) NOT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Persona`
--

INSERT INTO `Persona` (`idPersona`, `Nombre`, `Apellido`, `Direccion`, `FirmaContratoPersona`, `Profesion_idProfesion`, `Pais_idPais`, `Departamento_idDepartamento`, `Ciudad_idCiudad`, `TipoDocumento_idTipoDocumento`) VALUES
(1019042184, 'Luis Daniel', 'Gordo Navas', 'Calle 2 #93D - 30', 'Luis Danniel Gordo Navas', 3, 169, 5, 149, 1),
(1019042186, 'Leslye Juliet', 'Navarro Ortega', 'Calle 2 93D-30 Torre 32 Apto 501', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAEsCAYAAAA1u0HIAAAgAElEQVR4Xu2dCfB313jHv6at6dDYhiASxNqJNZJB0EnVUktDUZTUEktSlIha0pgi0yKxhNgaS6Iae5BaqjQhFEWbFB1rKEUkGkaUidYM085X74mTk3t/997fXX7nnvs5M++Q93/vOef5POf9f+855znPuZwoEIAABCAAAQgsnsDlFm8BBkAAAhCAAAQgIASdQQABCEAAAhAogACCXoATMQECEIAABCCAoDMGIAABCEAAAgUQQNALcCImQAACEIAABBB0xgAEIAABCECgAAIIegFOxAQIQAACEIAAgs4YgAAEIAABCBRAAEEvwImYAAEIQAACEEDQGQMQgAAEIACBAggg6AU4ERMgAAEIQAACCDpjAAIQgAAEIFAAAQS9ACdiAgQgAAEIQABBZwxAAAIQgAAECiCAoBfgREyAAAQgAAEIIOiMAQhAAAIQgEABBBD0ApyICRCAAAQgAAEEnTEAAQhAAAIQKIAAgl6AEzEBAhCAAAQggKAzBiAAAQhAAAIFEEDQC3AiJkAAAhCAAAQQdMYABCAAAQhAoAACCHoBTsQECEAAAhCAAILOGIAABCAAAQgUQABBL8CJmAABCEAAAhBA0BkDEIAABCAAgQIIIOgFOBETIAABCEAAAgg6YwACEIAABCBQAAEEvQAnYgIEIAABCEAAQWcMQAACEIAABAoggKAX4ERMgAAEIAABCCDojAEIQAACEIBAAQQQ9AKciAkQgAAEIAABBJ0xAAEIQAACECiAAIJegBMxAQIQgAAEIICgMwYgAAEIQAACBRBA0AtwIiZAAAIQgAAEEHTGAAQgAAEIQKAAAgh6AU7EBAhAAAIQgACCzhiAAAQgAAEIFEAAQS/AiZgAAQhAAAIQQNAZAxCAAAQgAIECCCDoBTgREyAAAQhAAAIIOmMAAhCAAAQgUAABBL0AJ2ICBCAAAQhAAEFnDEAAAhCAAAQKIICgF+BETIAABCAAAQgg6IwBCEAAAhCAQAEEEPQCnIgJEIAABCAAAQSdMQABCEAAAhAogACCXoATMQECEIAABCCAoDMGIAABCEAAAgUQQNALcCImQAACEIAABBB0xgAEIAABCECgAAIIegFOxAQIQAACEIAAgs4YgAAEIAABCBRAAEEvwImYAAEIQAACEEDQGQMQgAAEIACBAggg6AU4ERMgAAEIQAACCDpjAAIQgAAEIFAAAQS9ACdiAgQgAAEIQABBZwxAAAIQgAAECiCAoBfgREyAAAQgAAEIIOiMAQhAAAIQgEABBBD0ApyICRCAAAQgAAEEnTEAAQhAAAIQKIAAgl6AEzEBAhCAAAQggKAzBiAAAQhAAAIFEEDQC3AiJkAAAhCAAAQQdMYABCAAAQhAoAACCHoBTsQECEAAAhCAAILOGIAABCAAAQgUQABBL8CJmAABCEAAAhBA0BkDEIAABCAAgQIIIOgFOBETIAABCEAAAgg6YwACEIAABCBQAAEEvQAnYgIEIAABCEAAQWcMQAACEIAABAoggKAX4ERMgAAEIAABCCDojAEIQAACEIBAAQQQ9AKciAkQgAAEIAABBJ0xAAEIQAACECiAAIJegBMxAQIQgAAEIICgMwYgAAEIQAACBRBA0AtwIiZAAAIQgAAEEHTGAAQgAAEIQKAAAgh6AU7EBAhAAAIQgACCzhiAAAQgAAEIFEAAQS/AiZgAAQhAAAIQQNAZAxCAAAQgAIECCCDoBTgREyAAAQhAAAIIOmMAAhCAAAQgUAABBL0AJ2ICBCAAAQhAAEFnDEAAAhCAAAQKIICgF+BETIAABCAAAQgg6IwBCEAAAhCAQAEEEPQCnIgJEIAABCAAAQSdMQABCEAAAhAogACCXoATMQECEIAABCCAoDMGIAABCEAAAgUQQNALcCImQAACEIAABBB0xgAEIAABCECgAAIIegFOxAQIQAACEIAAgs4YgAAEIAABCBRAAEEvwImYAAEIQAACEEDQGQMQgAAEIACBAggg6AU4ERMgAAEIQAACCDpjAAIQgAAEIFAAAQS9ACdiAgQgAAEIQABBZwxAAAIQgAAECiCAoBfgREyAAAQgAAEIIOiMAQhAAAIQgEABBBD0ApyICRCAAAQgAAEEnTEAAQhAAAIQKIAAgl6AEzEBAhCAAAQggKAzBiAAAQhAAAIFEEDQC3AiJkAAAhCAAAQQdMYABCAAAQhAoAACCHoBTsQECEAAAhCAAILOGIAABCAAAQgUQABBL8CJmAABCEAAAhBA0BkDEIAABCAAgQIIIOgFOBETIAABCEAAAgg6YwACEIAABCBQAAEEvQAnYgIEIAABCEAAQWcMQAACEIAABAoggKAX4ERMgAAEIAABCCDojAEIQAACEIBAAQQQ9AKciAkQgAAEIAABBJ0xAAEIQAACECiAAIJegBMxAQIQgAAEIICgMwYgAAEIQAACBRBA0AtwYsYm/KGkwyRdVdJJkk7JuK90DQIQgMCiCSDoi3Zf9p3/pKTbR728s6SPZN9rOggBCEBggQQQ9AU6bUFd/t+kr8dLOnpB/aerEIAABBZDAEFfjKsW2dGvS9o36vl5kvZZpCV0GgIQgEDmBBD0zB208O59R9JeiQ0vkvS0hdtF9yEAAQhkRwBBz84lRXXoDEl3rbFob0kWewoEIAABCIxEAEEfCSTV1BJ4taTDa37yIEmnwQwCEIAABMYjgKCPx5KaLkvAYm5RTwt76YwWCEAAAiMTQNBHBkp1lyLg5XYvu9eVv67OqIMMAhCAAARGIICgjwCRKjYSiI+u/VzSr0RPP1TSW+AHAQhAAALDCSDowxlSw2YCn5F064ZHzpF0IACzIuBtkodI+m1J75V0JUn/WZ1M+FZWPaUzEIDApQgg6AyIqQm8XtIjo0Z+VIlE+CuSzUztge71n1UJedMbf1WJ/N93r3KSJ68t6QBJ95T0E0kXS3rOJC1RKQQWRABBX5CzFtrVJ0t6SdR3z/oOif6bALk8HOutD+febyvfk/SEHZ5SuL4kf3j4f+Oy6361cePnEJicAII+OeLVN+ClW/8CjovzufvvQ3mNpCNWT2q3ANI0vW29OXZHs+IXtCQm8v0Bd2jrPD+HQIkEEPQSvZqfTWdXS6ShZ/eW9HdJNxmLu/Obl6ufHTX/xWoWfqGkP65mw/GqSnj0REneMrlgpq5fUdI3JF2jpT1/ML5q4lUEL/c7n8LnJTkWhEuHZhoENNNMgF+ijI45CPyNpIdFDVkIri7p0OjvnGjGvyAp8xNI8wU8XdILk248UdLLaro2t9/ilYR/kfROScc1IJtiFeFGkp6ZxIW8VNJR87uNFiFwaQIIOiNiDgK/L+n0qCHP/H6nmtVY2EPZT9KX5ugQbVyKQJovwLNb75OnxTP5R9TsX/sSnlNnWIJP+xkE21H5JyXBlqHvY2YlTFcyQhtzf9QwfCFQSwBBZ2DMQeAqki5KGrpq9Uv4wdHfO3juKXN0iDYuRcC++Eq0lP1ZSfs3MLqHJK+w3KTm51P7L00lfJgkJygK5e2SHpj0y8FyFvVtl8S9vO94j+dJ8uw8LV+rZufvY0xBYNcEEPRde2A97VskbhWZ61/Gn5B0boLAvzw/uh4s2VjqvekQOf4fybW3aSeDyFlg/TEQF7/7hglm677Q59tRQxbq35T0g+jvbiDp4Uk8QPjxNr/rPPO3kKcR9aHOKZb0sxkQdGR5BLYZ5Muzkh7nQCBdrny3JC/Fp7MuzqXvxltplHvX3w1pnoHQ+1dKeu6IAXOfknS7CI23cO7fgKruUqA+y+L+qHSQYHwSI27KdXmcOhiOAoFsCHT9R5tNh+nIYgk4W5yzxsXFs7sbS/rn6C85l35ZF4dEKg+oZos+5jdmylzPQD1DD8WrJjftMdKaAubGWoL3MrqX00P5fiW2X9jQxzpRb/t95w+GRzXcEOimLOT+gNl1Yp0eruHRNRFoG+BrYoGt0xPwcuz1ombCHmi698nFLb+EZDH3B4+XnOPiCO/3VyscQ4+Npasn3iN3QqA+xbPZx9fsYfeZGTe15yDK+Khal1WctD+b+uFnHRvwjIYOOOWttxGcKW8o6z5MeRYCvQgg6L1w8fBAAj7ec2RUhwOV7lyl8DxF0rWin3Fxy//D8NG+N27gPoZgPl/S0VEb2+4NX606RpYuVTsK/tFbBqalH3t98v975cF98Yze76VivCkWwDi8WnRy1e9tg+oG/pPhdQh0J4Cgd2fFk8MJ1C277yvJM/f0rHoQ++GtLruGtvzqts7Z0by8bXHfpqRt3FCSRXib4pWEg6oTDBb4uPQ9QpZuBbiusT70LPRePq8LeLOQvyn5yNmGBe9AYFYCCPqsuGlMUhrtHpZ3b1ZzLn2sPdglg0/F1glMnNo0PZ5lGz2T//PqA6mPzdsGxG1qw8v4j5W0V/JQn9l/GnD3YUl36WNYzbM+Qul6HZBZVxyf4D3yvx3YDq9DYHYCCPrsyFffYHpZyw+jo09p8JNheUl+zcud6f62BdEBX859X5fkZZuPIN+At0c1Mn/ckKBlm4FrfzraPU3V6j1wf8i17UenHxpOPzvkvHc69mKbCHjbxsO8kxUBBD0rd6yiM3VJZuIEIWl0speTj1mxqKeX28RZ3JxP3ClavbqRipP3xLsum/t8dwi6G/uUwe9VqVnr+uhtliaBvp+kd0VGDYkV2LS87iY8vhyI2faBsYp/oBi5XAII+nJ9t+Se+5enZ5ehxIlM6qK6u0Q1L5nHpr77A8jHs8LSdSpse0rymf7bJ5X4nZt3hJLOhKdYFanL4mZbfJ687ghe+rzPnMfpg7uY5v1xr1g0La87ct2zdq8SUSCweAII+uJduEgD6oKdQnCcDapbeve+sWfrayy+0SvMcJ1d704JhOtKum/N5Sldr6WNs8S5aqd9dazDmMU+9951XbIWH797TDRDTlPROiucx8fFPTpkofaJirqgt29WmezitLE9quZRCORJAEHP0y9r6JX3xQ+ODPVs6ZHRf6dR713FaU52IeHLgVVAmvN6T1G6Bq3VZW3r8iHUtf6htjk1q4P2Yj+HOi2ujg/wao3Ps3vvPZSXS3pSx8Y3fTj8lyQfnfQfZuUdgfLYcggg6MvxVWk9TW9gi4PjbKsv//CFIXGJZ/E58LCA+4hXKJ5F+y7xsYtnpleoKv2JJN8L3lQshBbEUNwf31LWtD/sj5Lzo+f7nPPe1s6mYLn3SrqPpDOqPof679UhO5u3JryN4yBC//+0fK5aXl9zgOW2/uK9hRBA0BfiqEK72ZQ5Lpj7oeqa1fDffY48TY3sAElnJ400XTs6tC/eagh75M5p7nPeTaWuX+ZoUa8r6YfVOxqOxA21IX3fov6XNbe2WXjjS3z8XtvvqbZZuUXes3IKBIom0PYPpWjjMW7nBJoubAkdu5ukf4h62XYL2JwG1d2N7b1eB6mNXfzhYKF26TKD9pK2l9/j0pTUJbXD96D7w2SO4iX4P6muH21qr+0jbtNRNN/aZxYeNxQIFE8AQS/exVkbWBcc54CoeH/TM7ZbRlb4ysx0KX4XRjbdMuZ84C8YuUPb7HE7JsFXiYbij43bVOlM4+6liWvaBHRk035RXVM2vE1xEw6us40OCEyL98ot5CSHmcJb1JktAQQ9W9espmNpcFw6Q6xLrOK/23U5oWFmOUXK2m0E3Xzimb3/++OSPFOP99PT42FTHFnr4qu0rz6i9tSGs/RePvd+ebpXbiG3iNcF3XXpA89AYNEEEPRFu6+IzqfLw75F7LaRZb7G88vRf/c5Xz0loKYZutv08vi/jth4fKysz7ZDmpTGXUo/OOIjcf75PjWz+BFNaazKfX2YJOf7t4+fVbNU7v1+c68LevPyuj/0CHqbw1u0kSUBBD1Lt6yqU2nmOKchvXJC4N8leb81FO9Tewl5l6UuUUroT4jWHqt/sej2/aCpO9MfL2V/V9I1q4522Z8fy6Y+9bTlX/exNn8AcBStD1WeLY4Agl6cSxdpkJdJnRglFKf9jPc/03SwDpY7c8eWbhJ0d83LxS8eqY/bztDdvMXwbZLunvTFe+WezXr/eqoPkTHM9wqOs701zcoJehuDMnUUQQBBL8KNizciXXZPk8ykP88hyUx63tt9OjzyxKdr0rFu66ht99BDe9epPjAcER6Kz54713ucdW4XAXFNTBww6X10L8GnxXvlXl53Mhpm5duOKt4rjgCCXpxLF2lQuuyeJpmxUUNFbWww8apBOK4WX3Li9ra5+ayun9+JcrlbiC3Q2xTnfHfilqaSS+Ief3g8m1n5Ni7mnTUTQNDX7P28bG9bdvcd1feIuryraOzQhboPjOMk+dhaKL6K1DPgfxuIuk9imU1NeT/dQpnefOZ3cpidkyBm4EDh9XUTQNDX7f+crG9bdveetK8KDWWs2e82DNJ0qedKcjS+S3qm+s2SDt2mkeid+Cz+0GNxvs7UQXtxeYWkJw7s45DX29K2emXBs3YSxAyhzLvFE0DQi3fxYgxsW3b3fd1e0g7Fv9ydcMaz4LlLmhDn5Oq2MPfjIZIs4nEZmuM9/kgYKujuV5cjYnMxbZuVkyBmLk/QzuIJIOiLd2FRBqTL7umyehpZvqto97ZkN+kZdQdvHTbAU3GU+5A99AFdmOTVTWlbPSu3mBP0Ngl6Ki2RAIJeoleXa1P6C/7Eaqk1WJSeqY5nxnNane6V1+VJTwPk7ijpn7bsZG4BgVuacclrpG0dSpD3IVBDAEFnWOREIF3KTrOi+dpQz1avUXXa0eWOzPb1onOWdAZeFx0+5iw9nqHbziX/u/VqhXMO1KVt9c/io3Vz+pS2ILB4Akv+xbB4+BhQS+CzyfWZ+0vy34XyOkmPjv7bS9kWgjlLGvhWl7nuDpI+EXXq+9XetTO99S3xVoM/YhyAd1HfSnb8PGlbd+wAmi+fAIJevo+XZmG6P31Ucpe1l659yUgoYwSJ9WXUdQk8naVvG/E+dlBcX3uHPO9VFx+Va7owhbStQ+jyLgQiAgg6wyE3As4M9pmoU750w3uucdn1EnQs6PGRtZTlftVFI/Hfb5O8JU5is4sPmG3HiJfPj5RkUU8Ld5VvS5X3INBAAEFnaORIwJHN8QUt6R3pzpH+lKjjcy+7x4KeBu6lPNM89NsIslckvDLh0vdyll341x9lzhOQfoi5LyFtq69ApUAAAiMSQNBHhElVoxHwnrjvuw4lvazFGeOcOS6U06p7vkfrQEtFsaB7yfhJLc+nEe91UfGbqui6xD+X/U3tONDNM/Km++qZle/aQ7RfNAEEvWj3Lta4tqxxNiwWOV8ycsOZrE2Pzh0t6fiWttNjbn0D5Hyl7B5VG06kc6WZbO3TjGfjnpU3Xabi5fe5gxf79J9nIbB4Agj64l1YpAFp1rj0+JqNTiPNh2Zj6woyDdo7QpJvWmsrZ0s6IHqoa4Cctxu+Julq1bufknRQW2Mz/ty+ctBb03Ez0rbO6AyaWjcBBH3d/s/Z+vT4WhpM9lxJx0QG9F3G3tb2tixxTfXWpYTtGiAXr0b4uJqPrfn42q6LV1Is5nVBb94rZ1a+aw/R/qoIIOircveijHXQlPdjQ0mPr6WXjAxNr9oVju88d6BbKH0+JLZNNpPbHvqm/Ovmkt5n35Utz0EAAgMIIOgD4PHqpAS8J+tl9VC8dOvkJHHZhdClM/QDJZ3Tg0Ta57b30+x5bqrrzL5Htzo/uin/um+F888dyU+BAARmJoCgzwyc5noRiMXPR9m8nxyXr0i6SfQXN672m3s10vNhX+Hqq1xD6ftv6ARJXm0I5YPVxS0XNPQj/bDxY33b7Gli7eOb8q/7hXQFZYw2qQMCEOhBYBe/GHp0j0dXTsAzvYMjBmka2PSO9DnOo6cz9L7/hhwY5wC5uHi74NiG+77TJf65z6E76M3R63WZ3rxP7hvyPCvnVrSV/2PF/N0T6PvLaPc9pgdrIpAu7zoI7vkRgPQI2SlJnvcpWMV51R245yte+4pZ+lHgfjadpXc+eOeFD+Utkh46hWE1dabHB+NHfKbcdrC8PpMzaAYCbQQQ9DZC/HyXBNI0sF+S5HSqcYmX5X2+O9zENlW/x8ra9gFJv5t00jPhOANe3X51ekf8FHaauwP46s6Uuz2fu/fZ+r4fMlP0lTohAIGKAILOUMidQCzYTrASp4R13+Of/6AKGPNzU5WxAvGaLi35cnVL2xuToEDbc6Gka05lWHWlabhIJb3e1M06MNGzdoR8QidQNQS2JYCgb0uO9+Yi4D1a358dSrqP/h5Jh0Q/b4saH9pvfzSE4LzzJV1nYIVpgpxN1aUpcAc2fanXfYLAKwRNZ8ot5PYFBQIQyJQAgp6pY+jWJQTSZec0mjpNq9rnXHhfzF7O9yw5lHdI8j7+0JJGvtfVZ7vOmGB2bAG3kKdHAkMffKacoLehHuZ9CMxAAEGfATJNDCLQdp1qGrjlhDTxsbBBjde83Pdilq7tO8DMEf11N5Q5Ar7pwpOu9dc9Z6H2Envd8jpnyoeQ5V0I7IAAgr4D6DTZm0B6nWo8blPB3+Z60q4d2uZilq51h+fuVl004730fSSd2reCDs97Nm4hb7pIxR9FU3xAdOgaj0AAAtsSQNC3Jcd7cxJou051rEC1Npu2vZilrd65ft6WstVH0Txr93E8CgQgsDACCPrCHLbS7qbL6icmt3vNlTFu24tZdu22Tclh3DcniCHobddeon0IDCSAoA8EyOuzEEjzmXsG6Wj3UE6W9Kjov6fKGDfkYpZZQNU0sin3uoXcqx/+UOEo2q48RLsQGIkAgj4SSKqZnIDvRL9e1IqPjgURSve2m7KuDe2kb1mzqIcy9RG5If31jNvR63UBb67X0esWcnOlQAACBRBA0Atw4kpM6LOP/vUqsGxsNEPzuI/dn7r6HlGlx712Q2OkbJ3DC7QBgR0QQNB3AJ0mtyLQto8eB8b9RNKNJDXdYLZVB6oZraPDQ8np34+T7zg6vS4xjPv7zSrugOQw23qf9yCQOYGcfiFljoru7ZhA2z56fGmKuzrFcvgYF7OMjdHn1v2RUXd+3W35PLlXFhDysclTHwQyI4CgZ+YQurORwKZ99HQGP8UlJmNdzDKGm32G3HvkTULuVQpfovIyAt7GwE0dEMifAIKev4/o4S8JbNpHTxPMTHHN6Fzn3Tf53HZ6Rt6UqtWR656Re/mdAgEIrIgAgr4iZxdgap99dJs79vi+WNIVKo6eAV9xRqZtOdc5gjajM2gKAjkSGPsXXo420qdyCLTto0+ZYMYXs3w1ur71U5IOmgFtW1IYd8GJdjwj5wjaDA6hCQjkSgBBz9Uz9KuJQLqPvm8kZH8m6XnRi2MnmEmX3PeU9L2JXNV0X3rcnO8n9/I6qVoncgLVQmBJBBD0JXmLvpqAZ6JHNoh2mmDmNZKOGBHbHHvoXYScnOsjOpWqIFAKAQS9FE+uxw4Hg50emRvndd9b0rejn/mmsoePhMbR5GcldY0ZSd9VyD0j941yFAhAAAKXIoCgMyCWRqBtH32qWXTarrmNcda97fiZ2/GM3CsTnCVf2milvxCYkQCCPiNsmhqNwKZ99PdIOiRqaQzRDdWN+bHgFK2O2m86R46QjzZcqAgC6yCAoK/Dz6VZme6j3y+avab51h8n6aQRADjK/cKknr5BcY5Yt5D7BrSmFK1ByFlaH8FpVAGBNRFA0Nfk7XJs3XQefYwEMy+qUD01QrafpC8kCG8m6YsdsIb9ce//N91+5mqIWu8Ak0cgAIF6Agg6I2OJBPruo/cJXvP58ttVUD4t6fbV/98mKM7vOCK/Kaubq3ZCGO+Nc5XpEkcifYZARgQQ9IycQVd6EdiU190JYHzbWiieWd+8Q+11ov0gSb5ffR9J30rq+HVJP62p1zefWaC9WtBUQopWp7MN97p36CKPQAACEGCGzhgoi8CmvO7pHrstP7YS2TYKabY5P++ldSeQ2bSH7qV0z8aPirLJ1bXliHX33X8oEIAABEYjwAx9NJRUNDOBdB89FuzrSDov6Y+zqe3foY+/Jekfk+c8Q3dg3YeSv7fQOzAuRKxvqv4N1dEzsrp1cAKPQAAC/Qkg6P2Z8UYeBNLgN8984yNgr6+OhcW93UvSBR26f0I1044f/YCkeyTvflfStTbU981qJu4VA5bVO4DnEQhAYHsCCPr27Hhz9wQskleOuhGPZ18x6n3suFhYvSTeVhwU5+C4uHjPO25rUx3e33+lpNch5G2o+TkEIDAWAQR9LJLUswsCToF6cNRwHM1+J0kfSzp1jKTnd+zo2ZIO6PhseMzHzvzRQGrWnuB4HAIQGE4AQR/OkBp2RyBNIuPZtwXVxUFqninHs+o+gu4c8K+VdPkW88KyuoPcuL50d2OBliGwegII+uqHwKIBpMfMPEOOz3yfIemukYVOGPO0DRb7fLuPnDngbtORM1fxP5KchY5o9UUPIToPgXIIIOjl+HKtlsT51b2nftUIRDqDrzuP7pm8RdwfApsSwNTx5d/PWkcddkMgQwL8QsrQKXSpF4F0H91H08LRsOMlPT2q7XxJPtLmEmbifUU8VOeZ+WG9esrDEIAABCYkgKBPCJeqZyGwaR89/Zk7ZCFuy6nu57w37pSsPnvuG9zSEl8IM4uhNAIBCEBgEwEEnfGxdALpProTuHgP3CVNPtNma8irbtGPI9XPSs64e6bvpDKcLW8jys8hAIHZCCDos6GmoYkIeA/8oqhuR5r7+JqX1I9uSfwSXvNHgGfj/tNUPNt3FP3nJd1xIluoFgIQgMDWBBD0rdHxYkYEvGd+q579cUR8EHFm2j3h8TgEIJAfAQQ9P5/Qo34EvOT+Ykm36fDaz6vscW/kzHgHWjwCAQgsigCCvih30dmKwH0kOSgt7JV3BeMb026aLNF3fZfnIAABCGRNAEHP2j10LiLgmbjzszsd6x4tZH4m6U2SPl5le4sfj9PDAhgCEIBAMQQQ9GJcWZwhe0s6qDKzLsMAAAlWSURBVMr0dvgW1oWxHd9vfm41Q9+iOl6BAAQgkDcBBD1v/6yxdz4j7gj1QyX9WgsAR7d/TpKvTvUNafH1pvFMPCSXecEagWIzBCCwDgII+jr8nLuVQcS7JHyxLW+r/pwp6ceVcemZ87+Q9KzcDad/EIAABMYigKCPRZJ6+hLwnvgjOmZtczCb87C/UNL7GxryZSqfiX7m8+K36NspnocABCCwVAII+lI9t8x+W3SDiPtms7byaUmnSHp7x6xs8UUtrpvx3UaYn0MAAsUQ4BdeMa7M1hAL95HVTLyLiA9J+PJOSfePSOzLefNsxwUdgwAERiaAoI8MlOp+QaDPveJ+foiIx8iPk/SM6C8eJOk0fAIBCEBgDQQQ9DV4eR4bw73iDk7z/nhbcXS6L0Fx+lXnXx+jpBe1vLTKvz5G3dQBAQhAIGsCCHrW7llE5xyZHvbF2zo8hYjHbe5XBc+Fv3OQXJeUsG395ucQgAAEsieAoGfvoiw76LSrPvPdJeFLuFfcs3FfojJ1+Ua15B/a2VOSo+QpEIAABIomgKAX7d7RjLuepLtJenCVua2tYt8rbgGfS8Tj/pwq6Y+ivzis6kdbn/k5BCAAgUUTQNAX7b5JO+/9aP9x+tW7d2jJIh6uI910r3iHqgY94nvLnfM9lGOrG9YGVcrLEIAABHIngKDn7qH5++fl9CdJOlDSb3Ro/sOSPCu2iOdwr3gaGPcaSUd0sINHIAABCCyaAIK+aPeN2vkXSXpAsv/c1ICX0r037tlwbuUaknwhy1Wrjn1d0g1z6yT9gQAEIDA2AQR9bKLLqs+zWV+E8nhJl2/pulOvvqPajx7rmNlUtJz29WZR5SSYmYo09UIAAtkQQNCzccVsHbliFZ3uILd7trT6keomMy+nzxGhPhaEE5Lz5wj6WGSpBwIQyJYAgp6ta0bvmM9oO+mL/3hZuql8t8qu9i5JFvQlFh+ne3XU8SdIetUSDaHPEIAABLoSQNC7klruc9eWdEgV+b3XBjN+JOlDSS70pVrtZDenR533nr+Pr1EgAAEIFEsAQS/WtZcY5pvKHthg5nmSPibpxZLOKQiFP2LOj+xxhjrf9EaBAAQgUCwBBL1Y1/7CMAeyeak9Lc6c5nvFvaT+QUkXFIghvkr14o5H8ArEgEkQgMBaCCDoZXs6vR+8zlqL3UmSfF773IJw+GPl4MgeAuMKci6mQAAClyWAoJc9Ki5sCYCLrfdRtDdkerZ8Gy/5pjXfwx6KE+bsMoPdNjbwDgQgAIHOBBD0zqgW+aATxTgN6i0kfVuSA99c4jPaqWFnVrP1T0ryHvtSi6P5Xx91nhSwS/Uk/YYABDoRQNA7YSruISeUuYmkx20IFvuppLdIepOkny3wCJuD4Hx9aigf7XhPe3HOxiAIQGAdBBD0dfi5yco9qn3m41pm7eF9i/tbq4j4JQTSxTEEToyz/7rdjfUQgEDJBBD0kr3bz7ajJD1F0t4dXguBdK+QlHMaWPfNV7+Gwnjv4FwegQAElkmAX3DL9NtUvfb57XtJeoiku3RsxElbTqvOs/+44ztzPZZGunuGvqQUtnNxoh0IQKAAAgh6AU6c0ATvtf+qpEMl3bslYt7BdKdU++4TdqlX1US698LFwxCAwJIJIOhL9t68fffs/QBJf9oSXPbe6n50z9p3XZ4s6SVRJ4h037VHaB8CEJiMAII+GdqiK75mFSH/BxuC6Szo/uNlb2em20XxCsNZUcPvluQ87xQIQAACxRFA0Itz6awG+dY254n3rP0GDS07MO2YHS3FX0XSRVG/iHSfdXjQGAQgMCcBBH1O2uW2FW50e7ykWzWY6bSyPh73gZlzx/9Q0pWjPjHmyx2HWAaBVRPgl9uq3T+J8Rb1Z0i6bkPtvjDGKWZfOEnrl62USPeZQNMMBCCwWwII+m75l9p6CKA7WdKeDUb62taXS5o6xexzqvS3oRu+F91H7SgQgAAEiiKAoBflziyN8QUpPtN+SEPvpt5jdxDc6VHbzu/+qCxJ0SkIQAACAwgg6APg8WovAveR5Gx0jjyvK14a9xWuzh8/Zrm+pG9EFX6+uqxmzDaoCwIQgMDOCSDoO3fB6jrgqPjwp854H3U7vsoXPxac9F54xv1YZKkHAhDIhgC/2LJxxao64j123/TmP1dvsNxJYN4u6YsjkPFZ9HhlgBSwI0ClCghAIC8CCHpe/lhjb3yU7WGS9qox3mfIvQx/9EAwzhbnrHGhHFHVO7BaXocABCCQDwEEPR9frLkn3ud+ZBKNHvM4r7oJbtusc17i92w/FKen9Z4+BQIQgEAxBBD0YlxZhCE3kvTMStzrDNo2It4XzPjK18tHlTL2ixgyGAEBCAQC/FJjLORIwML+2paI+MdK+lqPzvtMfHxc7cCRA+96dIVHIQABCIxPAEEfnyk1jkegLSLeCWKcdc5L8W3Fd7y/OXrI//3Wtpf4OQQgAIGlEEDQl+Kp9fbTEfHPkuSb3TZFxDsj3KbiWf9XowecW/6m68WK5RCAQGkEEPTSPFq2PY6Id574unKmpDe1pHXlPHrZ4wPrILBqAgj6qt2/SOPvKenpG/bXHTjnn9dFxDvtq6PpQ3E62vctkgKdhgAEIJAQQNAZEksl8ERJL9vQ+f+W5PPmp0bPpMfXTqqS2yyVAf2GAAQgcAkBBJ3BsGQC+1Uz9VduMMKR8CdKekX1jHO536z6/+dIcrQ7BQIQgMDiCSDoi3chBlQEHBT3aEl7NxCxeHt5/b6Sbh09c+eOUfKAhgAEIJA1AQQ9a/fQuZ4EutzDnlZJGtiekHkcAhDIkwCCnqdf6NVwAodKesyG4LnQAoFxw1lTAwQgkAEBBD0DJ9CFSQk4gczhDcLuHPG3lXTBpD2gcghAAAIzEEDQZ4BME1kQuKWk+0s6WJKPtr2zSv2KmGfhHjoBAQgMJYCgDyXI+xCAAAQgAIEMCCDoGTiBLkAAAhCAAASGEkDQhxLkfQhAAAIQgEAGBBD0DJxAFyAAAQhAAAJDCSDoQwnyPgQgAAEIQCADAgh6Bk6gCxCAAAQgAIGhBBD0oQR5HwIQgAAEIJABAQQ9AyfQBQhAAAIQgMBQAgj6UIK8DwEIQAACEMiAAIKegRPoAgQgAAEIQGAoAQR9KEHehwAEIAABCGRAAEHPwAl0AQIQgAAEIDCUAII+lCDvQwACEIAABDIg8H8Jo2p4HJr3IQAAAABJRU5ErkJggg==', 3, 169, 5, 149, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Profesion`
--

CREATE TABLE `Profesion` (
  `idProfesion` int(11) NOT NULL,
  `Profesion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Profesion`
--

INSERT INTO `Profesion` (`idProfesion`, `Profesion`) VALUES
(1, 'Desarrollador Junior'),
(2, 'Desarrollador Medio'),
(3, 'Desarrollador Senior'),
(4, 'QA Junior'),
(5, 'QA medio'),
(6, 'QA Senior'),
(7, 'Recursos humanos'),
(8, 'Administrativo'),
(9, 'Servicios generales'),
(10, 'domiciliario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Sector`
--

CREATE TABLE `Sector` (
  `idSector` int(11) NOT NULL,
  `Sector` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Sector`
--

INSERT INTO `Sector` (`idSector`, `Sector`) VALUES
(1, 'Publicidad / RRPP'),
(2, 'Agricultura / Pesca / Ganadería'),
(3, 'Informática / Hardware'),
(4, 'Informática / Software'),
(5, 'Construcción / obras'),
(6, 'Venta al consumidor'),
(7, 'Educación'),
(8, 'Energía'),
(9, 'Entretenimiento / Deportes'),
(10, 'Finanzas / Banca'),
(11, 'Salud / Medicina'),
(12, 'Hostelería / Turismo'),
(13, 'Internet'),
(14, 'Medios de Comunicación'),
(15, 'Fabricación'),
(16, 'Gobierno / No Lucro'),
(17, 'Servicios Profesionales'),
(18, 'Materias Primas'),
(19, 'RRHH / Personal'),
(20, 'Venta al por mayor'),
(21, 'Telecomunicaciones'),
(22, 'Transporte'),
(23, 'Legal / Asesoría');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoContrato`
--

CREATE TABLE `TipoContrato` (
  `idTipoContrato` int(11) NOT NULL,
  `TipoContrato` longtext DEFAULT NULL,
  `Descripcion` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `TipoContrato`
--

INSERT INTO `TipoContrato` (`idTipoContrato`, `TipoContrato`, `Descripcion`) VALUES
(1, 'A termino indefinido', 'Contrato que cuenta sin una fecha final'),
(2, 'Contrato a termino definido', 'Contrato que cuenta con una fecha final '),
(3, 'Contrato por prestación de servicios', 'Contrato que se ejecuta con una tarea determinada '),
(7, 'Registro de prueba', 'contr');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoDocumento`
--

CREATE TABLE `TipoDocumento` (
  `idTipoDocumento` int(11) NOT NULL,
  `TipoDocumento` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `TipoDocumento`
--

INSERT INTO `TipoDocumento` (`idTipoDocumento`, `TipoDocumento`) VALUES
(1, 'Cedula de ciudadania'),
(2, 'Cedula de extranjeria'),
(3, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tipologia`
--

CREATE TABLE `Tipologia` (
  `idTipologia` int(11) NOT NULL,
  `Tipologia` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Tipologia`
--

INSERT INTO `Tipologia` (`idTipologia`, `Tipologia`) VALUES
(1, 'Agencia de reclutamiento'),
(2, 'Empleador directo'),
(3, 'Servicios temporales');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `AlmacenContratos`
--
ALTER TABLE `AlmacenContratos`
  ADD PRIMARY KEY (`idAlmacenContratos`),
  ADD KEY `fk_AlmacenContratos_TipoContrato1_idx` (`TipoContrato_idTipoContrato`);

--
-- Indices de la tabla `Cargo`
--
ALTER TABLE `Cargo`
  ADD PRIMARY KEY (`idCargo`);

--
-- Indices de la tabla `Ciudad`
--
ALTER TABLE `Ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD KEY `fk_Ciudad_Departamento1_idx` (`Departamento_idDepartamento`);

--
-- Indices de la tabla `Clausulas`
--
ALTER TABLE `Clausulas`
  ADD PRIMARY KEY (`idClausulas`);

--
-- Indices de la tabla `Contacto`
--
ALTER TABLE `Contacto`
  ADD PRIMARY KEY (`IdContacto`),
  ADD KEY `fk_Contacto_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`),
  ADD KEY `fk_Contacto_Cargo1_idx` (`Cargo_idCargo`),
  ADD KEY `fk_Contacto_Empresa1_idx` (`Empresa_nitEmpresa`);

--
-- Indices de la tabla `Contratos`
--
ALTER TABLE `Contratos`
  ADD PRIMARY KEY (`idContratos`),
  ADD KEY `fk_Contratos_TipoContrato1_idx` (`TipoContrato_idTipoContrato`),
  ADD KEY `fk_Contratos_FirmaContrato1_idx` (`FirmaContrato_Documento`),
  ADD KEY `fk_Contratos_Empresa1_idx` (`Empresa_nitEmpresa`),
  ADD KEY `fk_Contratos_Persona1_idx` (`Persona_idPersona`),
  ADD KEY `fk_Contratos_EstadoContrato1_idx` (`EstadoContrato_idEstadoContrato`),
  ADD KEY `fk_Contratos_Profesion1_idx` (`Profesion_idProfesion`);

--
-- Indices de la tabla `ContratosHasClausulas`
--
ALTER TABLE `ContratosHasClausulas`
  ADD PRIMARY KEY (`Contratos_idContratos`,`Clausulas_idClausulas`),
  ADD KEY `fk_Contratos_has_Clausulas_Clausulas1_idx` (`Clausulas_idClausulas`),
  ADD KEY `fk_Contratos_has_Clausulas_Contratos1_idx` (`Contratos_idContratos`);

--
-- Indices de la tabla `ContratosHasParametros`
--
ALTER TABLE `ContratosHasParametros`
  ADD PRIMARY KEY (`Contratos_idContratos`,`Parametros_idParametros`),
  ADD KEY `fk_Contratos_has_Parametros_Parametros1_idx` (`Parametros_idParametros`),
  ADD KEY `fk_Contratos_has_Parametros_Contratos1_idx` (`Contratos_idContratos`);

--
-- Indices de la tabla `Departamento`
--
ALTER TABLE `Departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD KEY `fk_Departamento_Pais_idx` (`Pais_idPais`);

--
-- Indices de la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`nitEmpresa`),
  ADD KEY `fk_Empresa_Sector1_idx` (`Sector_idSector`),
  ADD KEY `fk_Empresa_Tipologia1_idx` (`Tipologia_idTipologia`),
  ADD KEY `fk_Empresa_Pais1_idx` (`Pais_idPais`),
  ADD KEY `fk_Empresa_Departamento1_idx` (`Departamento_idDepartamento`),
  ADD KEY `fk_Empresa_Ciudad1_idx` (`Ciudad_idCiudad`);

--
-- Indices de la tabla `EstadoContrato`
--
ALTER TABLE `EstadoContrato`
  ADD PRIMARY KEY (`idEstadoContrato`);

--
-- Indices de la tabla `FirmaContrato`
--
ALTER TABLE `FirmaContrato`
  ADD PRIMARY KEY (`Documento`),
  ADD KEY `fk_FirmaContrato_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`),
  ADD KEY `fk_FirmaContrato_Empresa1_idx` (`Empresa_nitEmpresa`);

--
-- Indices de la tabla `Pais`
--
ALTER TABLE `Pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `Parametros`
--
ALTER TABLE `Parametros`
  ADD PRIMARY KEY (`idParametros`);

--
-- Indices de la tabla `Persona`
--
ALTER TABLE `Persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD KEY `fk_Persona_Profesion1_idx` (`Profesion_idProfesion`),
  ADD KEY `fk_Persona_Pais1_idx` (`Pais_idPais`),
  ADD KEY `fk_Persona_Departamento1_idx` (`Departamento_idDepartamento`),
  ADD KEY `fk_Persona_Ciudad1_idx` (`Ciudad_idCiudad`),
  ADD KEY `fk_Persona_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`);

--
-- Indices de la tabla `Profesion`
--
ALTER TABLE `Profesion`
  ADD PRIMARY KEY (`idProfesion`);

--
-- Indices de la tabla `Sector`
--
ALTER TABLE `Sector`
  ADD PRIMARY KEY (`idSector`);

--
-- Indices de la tabla `TipoContrato`
--
ALTER TABLE `TipoContrato`
  ADD PRIMARY KEY (`idTipoContrato`);

--
-- Indices de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  ADD PRIMARY KEY (`idTipoDocumento`);

--
-- Indices de la tabla `Tipologia`
--
ALTER TABLE `Tipologia`
  ADD PRIMARY KEY (`idTipologia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `AlmacenContratos`
--
ALTER TABLE `AlmacenContratos`
  MODIFY `idAlmacenContratos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `Clausulas`
--
ALTER TABLE `Clausulas`
  MODIFY `idClausulas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `Contratos`
--
ALTER TABLE `Contratos`
  MODIFY `idContratos` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `EstadoContrato`
--
ALTER TABLE `EstadoContrato`
  MODIFY `idEstadoContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `Sector`
--
ALTER TABLE `Sector`
  MODIFY `idSector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `TipoContrato`
--
ALTER TABLE `TipoContrato`
  MODIFY `idTipoContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  MODIFY `idTipoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `Tipologia`
--
ALTER TABLE `Tipologia`
  MODIFY `idTipologia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AlmacenContratos`
--
ALTER TABLE `AlmacenContratos`
  ADD CONSTRAINT `fk_AlmacenContratos_TipoContrato1` FOREIGN KEY (`TipoContrato_idTipoContrato`) REFERENCES `TipoContrato` (`idTipoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Ciudad`
--
ALTER TABLE `Ciudad`
  ADD CONSTRAINT `fk_Ciudad_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Contacto`
--
ALTER TABLE `Contacto`
  ADD CONSTRAINT `fk_Contacto_Cargo1` FOREIGN KEY (`Cargo_idCargo`) REFERENCES `Cargo` (`idCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contacto_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contacto_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Contratos`
--
ALTER TABLE `Contratos`
  ADD CONSTRAINT `fk_Contratos_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_EstadoContrato1` FOREIGN KEY (`EstadoContrato_idEstadoContrato`) REFERENCES `EstadoContrato` (`idEstadoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_FirmaContrato1` FOREIGN KEY (`FirmaContrato_Documento`) REFERENCES `FirmaContrato` (`Documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `Persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_Profesion1` FOREIGN KEY (`Profesion_idProfesion`) REFERENCES `Profesion` (`idProfesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_TipoContrato1` FOREIGN KEY (`TipoContrato_idTipoContrato`) REFERENCES `TipoContrato` (`idTipoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ContratosHasClausulas`
--
ALTER TABLE `ContratosHasClausulas`
  ADD CONSTRAINT `fk_Contratos_has_Clausulas_Clausulas1` FOREIGN KEY (`Clausulas_idClausulas`) REFERENCES `Clausulas` (`idClausulas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_has_Clausulas_Contratos1` FOREIGN KEY (`Contratos_idContratos`) REFERENCES `Contratos` (`idContratos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ContratosHasParametros`
--
ALTER TABLE `ContratosHasParametros`
  ADD CONSTRAINT `fk_Contratos_has_Parametros_Contratos1` FOREIGN KEY (`Contratos_idContratos`) REFERENCES `Contratos` (`idContratos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_has_Parametros_Parametros1` FOREIGN KEY (`Parametros_idParametros`) REFERENCES `Parametros` (`idParametros`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Departamento`
--
ALTER TABLE `Departamento`
  ADD CONSTRAINT `fk_Departamento_Pais` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD CONSTRAINT `fk_Empresa_Ciudad1` FOREIGN KEY (`Ciudad_idCiudad`) REFERENCES `Ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Pais1` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Sector1` FOREIGN KEY (`Sector_idSector`) REFERENCES `Sector` (`idSector`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Tipologia1` FOREIGN KEY (`Tipologia_idTipologia`) REFERENCES `Tipologia` (`idTipologia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `FirmaContrato`
--
ALTER TABLE `FirmaContrato`
  ADD CONSTRAINT `fk_FirmaContrato_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_FirmaContrato_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Persona`
--
ALTER TABLE `Persona`
  ADD CONSTRAINT `fk_Persona_Ciudad1` FOREIGN KEY (`Ciudad_idCiudad`) REFERENCES `Ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Pais1` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Profesion1` FOREIGN KEY (`Profesion_idProfesion`) REFERENCES `Profesion` (`idProfesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
