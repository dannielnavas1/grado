-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-11-2019 a las 21:00:46
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fwGrado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Permissions`
--

CREATE TABLE `Permissions` (
  `idPermissions` int(11) NOT NULL,
  `Permissions` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Roles`
--

CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL,
  `Rol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Roles`
--

INSERT INTO `Roles` (`idRoles`, `Rol`) VALUES
(2, 'Administrador'),
(3, 'Empresa'),
(4, 'Persona');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Roles_has_Permissions`
--

CREATE TABLE `Roles_has_Permissions` (
  `Roles_idRoles` int(11) NOT NULL,
  `Permissions_idPermissions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoDocumento`
--

CREATE TABLE `TipoDocumento` (
  `idTipoDocumento` int(11) NOT NULL,
  `TipoDocumento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `TipoDocumento`
--

INSERT INTO `TipoDocumento` (`idTipoDocumento`, `TipoDocumento`) VALUES
(1, 'Cedula de ciudadania'),
(2, 'Cedula de extranjeria'),
(3, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Users`
--

CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL,
  `Email` longtext NOT NULL,
  `Name` longtext NOT NULL,
  `Password` longtext NOT NULL,
  `Celular` varchar(45) DEFAULT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL,
  `Roles_idRoles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Users`
--

INSERT INTO `Users` (`idUsers`, `Email`, `Name`, `Password`, `Celular`, `TipoDocumento_idTipoDocumento`, `Roles_idRoles`) VALUES
(1019042184, 'dannielnavas@gmail.com', 'Luis Daniel Gordo Navas', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3163418533', 1, 2),
(1019042185, 'ldannynavas@gmail.com', 'Luis Daniel Gordo Navas', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3163418533', 1, 2),
(1019042186, 'leslye27navarro@gmail.com', 'Leslye Juliet Navarro Ortega', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3208493462', 1, 2),
(1019042187, 'danniel13navas@icloud.com', 'Luis Daniel Gordo Navas', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3163418533', 1, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Permissions`
--
ALTER TABLE `Permissions`
  ADD PRIMARY KEY (`idPermissions`);

--
-- Indices de la tabla `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`idRoles`);

--
-- Indices de la tabla `Roles_has_Permissions`
--
ALTER TABLE `Roles_has_Permissions`
  ADD PRIMARY KEY (`Roles_idRoles`,`Permissions_idPermissions`),
  ADD KEY `fk_Roles_has_Permissions_Permissions1_idx` (`Permissions_idPermissions`),
  ADD KEY `fk_Roles_has_Permissions_Roles_idx` (`Roles_idRoles`);

--
-- Indices de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  ADD PRIMARY KEY (`idTipoDocumento`);

--
-- Indices de la tabla `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`idUsers`),
  ADD KEY `fk_Users_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`),
  ADD KEY `fk_Users_Roles1_idx` (`Roles_idRoles`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Permissions`
--
ALTER TABLE `Permissions`
  MODIFY `idPermissions` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Roles`
--
ALTER TABLE `Roles`
  MODIFY `idRoles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  MODIFY `idTipoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `Users`
--
ALTER TABLE `Users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1019042188;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Roles_has_Permissions`
--
ALTER TABLE `Roles_has_Permissions`
  ADD CONSTRAINT `fk_Roles_has_Permissions_Permissions1` FOREIGN KEY (`Permissions_idPermissions`) REFERENCES `Permissions` (`idPermissions`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Roles_has_Permissions_Roles` FOREIGN KEY (`Roles_idRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Users`
--
ALTER TABLE `Users`
  ADD CONSTRAINT `fk_Users_Roles1` FOREIGN KEY (`Roles_idRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Users_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
